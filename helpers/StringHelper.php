<?php
namespace emilasp\core\helpers;

use yii\helpers\Html;

/**
 * Class StringHelper
 * @package emilasp\core\helpers
 */
class StringHelper extends \yii\helpers\StringHelper
{
    /**
     * @param $text
     * @param string $delimiter
     *
     * @return array
     */
    public static function stringParts($text, $delimiter = "\n")
    {
        $parts = explode($delimiter, $text);
        return $parts;
    }


    /** Переводим строку в транслит и заменяем символы невалидные для url
     *
     * @param $str
     * @return mixed|string
     */
    public static function str2url($str) {
        // переводим в транслит
        $str = self::rus2translit($str);
        // в нижний регистр
        $str = strtolower($str);
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        $str = preg_replace('/([_-])\\1+/', '$1', $str);
        // удаляем начальные и конечные '-'
        $str = trim($str, "-");
        return $str;
    }

    /**
     * Генерируем уникальное имя для файлов в частности
     *
     * @param bool|string $ext
     * @param bool|string $title
     * @param integer $words
     *
     * @return string
     */
    public static function generateName($title = false, $ext = false, $words = 5)
    {
        if ($title) {
            $name = self::str2url($title);

            $arrWords = explode('-', $title);

            $_name = '';
            for ($i = 1; $i <= $words; $i++) {
                if (!isset($arrWords[$i])) {
                    break;
                }
                $_name .= '-' . $arrWords[$i];
            }

            if (strlen($_name) > 5) {
                $name = $_name;
            }

        } else {
            $name = time();
        }

        $name = $name . (($ext) ? '.' . $ext : '');

        return $name;
    }

    /** Переводим кирилицу в транслит
     *
     * @param $string
     *
     * @return string
     */
    public static function rus2translit($string)
    {
        $translit = [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'yo',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'j',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            //'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'shch',
            'ь' => '',
            'ы' => 'y',
            'ъ' => '',
            'э' => 'eh',
            'ю' => 'yu',
            'я' => 'ya',
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ё' => 'YO',
            'Ж' => 'Zh',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'J',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            //'Х' => 'H',
            'Ц' => 'C',
            'Ч' => 'CH',
            'Ш' => 'SH',
            'Щ' => 'SHCH',
            'Ь' => '',
            'Ы' => 'Y',
            'Ъ' => '',
            'Э' => 'EH',
            'Ю' => 'YU',
            'Я' => 'YA',
        ];

        $string = strtr($string, $translit);

        $string = str_replace(
            ['kх', 'zх', 'cх', 'sх', 'eх', 'hх'],
            ['kkh', 'zkh', 'ckh', 'skh', 'ekh', 'hkh'],
            $string
        ); // k,z,c,s,e,h: kh

        $string = str_replace(['х', 'Х'], ['h', 'H'], $string);

        return $string;
    }

    /**
     * @param $time
     *
     * @return string
     */
    public static function timeLast($time)
    {
        if ($time > 86400) {
            return gmdate("d д.", $time - 86400) . gmdate(" h ч. i м.", $time);
        } else {
            return gmdate("h ч. i м.", $time);
        }
    }

    /**
     * Получаем имя класса объекта
     *
     * @param object $model
     * @param bool|string|array $delString
     * @param bool $strToLower
     *
     * @return string
     */
    public static function getClassName($model, $strToLower = true)
    {
        if (is_string($model)) {
            $className = StringHelper::basename($model);
        } else {
            $className = StringHelper::basename(get_class($model));
        }
        if ($strToLower) {
            $className = strtolower($className);
        }
        return $className;
    }

    /**
     * Обрезаем строку до нужно длины по словам
     *
     * @param string $str
     * @param int $maxLen
     * @param string $simbol
     * @param string $afterText
     *
     * @return string
     */
    public static function truncateString($str, $maxLen = 500, $simbol = ' ', $afterText = '')
    {
        if (mb_strlen($str) > $maxLen) {
            preg_match('/^.{0,' . $maxLen . '}' . $simbol . '.*?/ui', $str, $match);
            return $match[0] . $afterText;
        }
        return $str;
    }

    /**
     * Удаляем определённые символы/слова из строки
     *
     * @param string $string
     * @param string $symbols
     *
     * @return mixed
     */
    public static function deleteSymbols($string, $symbols = '\r\n\t')
    {
        return preg_replace('/[' . $symbols . ']+(?![^(]*\))/', "", $string);
    }

    /**
     * Очищаем и обрезаем строку для Мета тегов страницы
     *
     * @param string $description
     * @param int $length
     *
     * @return mixed|string
     */
    public static function toMeta($description, $length = 1000)
    {
        $description = htmlspecialchars(strip_tags(html_entity_decode($description)));
        //$description = preg_replace('%[^A-Za-zА-Яа-я0-9.-]%', '', $description);
        $description = self::deleteSymbols($description);
        $description = self::truncateString($description, $length);

        return trim($description);
    }

    /**
     * @param $string
     * @param string $delimiter
     * @param int $minLength
     *
     * @return string
     */
    public static function firstParagraf($string, $delimiter = '</p>', $minLength = 500)
    {
        $arr = explode($delimiter, $string);

        $arrNew = [];
        $len    = 0;
        foreach ($arr as $paragraph) {
            $arrNew[] = $paragraph;
            $len += strlen($paragraph);
            if ($len > $minLength) {
                break;
            }
        }

        if (count($arrNew) > 0) {
            return implode($delimiter, $arrNew) . $delimiter;
        }
        return $string;
    }

    /** Простое шифрование числа
     *
     * @param $num
     * @param $guid
     *
     * @return string
     */
    public static function encodeNumber($num, $guid)
    {
        $symArr = [
            '',
        ];
    }

    /** Дешифровка чисел
     *
     * @param $enc
     * @param $guid
     *
     * @return string
     */
    public static function decodeNumber($enc, $guid)
    {
        $data  = explode('-', $enc);
        $cnt   = count($data);
        $old   = 0;
        $fib   = $guid;
        $numbs = [];
        $i     = 0;
        while ($i < $cnt) {
            $fib_old = $fib;
            $fib     = $fib + $old;
            $s       = (int)$data[$i];
            $numbs[] = ($s - $fib) / (1 + $fib);
            $old     = $fib_old;
            $i++;
        }
        return implode('', $numbs);
    }

    /** Шифруем строку по ключу
     *
     * @param $str
     * @param $key
     *
     * @return string
     */
    public static function encode($string, $key, $salt = null)
    {
        $iv = mcrypt_create_iv(
            mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
            MCRYPT_DEV_URANDOM
        );

        $encrypted = self::url_base64_encode(
            $iv .
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_128,
                hash('sha256', $key, true),
                $string,
                MCRYPT_MODE_CBC,
                $iv
            )
        );
        return $encrypted;
    }

    /** Декодируем зашифрованную строку по ключу
     *
     * @param string $string
     * @param string $key
     *
     * @return mixed
     */
    public static function decode($string, $key, $salt = null)
    {
        $data = self::url_base64_decode($string);
        $iv   = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

        $decrypted = rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128,
                hash('sha256', $key, true),
                substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
                MCRYPT_MODE_CBC,
                $iv
            ),
            "\0"
        );

        return $decrypted;
    }

    public static function url_base64_encode($str)
    {
        return strtr(base64_encode($str),
            [
                '+' => '.',
                '=' => '-',
                '/' => '~',
            ]
        );
    }

    public static function url_base64_decode($str)
    {
        return base64_decode(strtr($str,
            [
                '.' => '+',
                '-' => '=',
                '~' => '/',
            ]
        ));
    }

    /**
     * Отдаём bage
     *
     * @param $value
     * @return string
     */
    public static function formatCheck($value, $trueText = 'Да', $falseText = 'Нет'): string
    {
        return $value ? '<span class="badge badge-success">' . $trueText . '</span>'
            : '<span class="badge badge-danger">' . $falseText . '</span>';
    }


    /**
     * Генерируем случайную строку
     *
     * @param int $length
     * @param string $characters
     * @return string
     */
    public static function generateRandomString($length = 10, $characters = null)
    {
        $characters   = $characters ?: '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    /**
     * Переводим массив
     *
     * @param string $category
     * @param array  $toTranslate
     * @return array
     */
    public static function yiiTranslateArray(string $category, array $toTranslate): array
    {
        array_walk($toTranslate, function (&$item) use ($category) {
            $item = \Yii::t($category, mb_convert_case($item, MB_CASE_TITLE));
        });

        return $toTranslate;
    }
}