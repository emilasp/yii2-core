<?php
namespace emilasp\core\helpers;

//use yii\helpers\StringHelper;

/**
 * Class ESiteHelper
 * @package emilasp\core\helpers
 */
class SiteHelper
{
    /**
     * @param bool $scheme
     *
     * @return string
     */
    public static function getDomein($scheme = true)
    {
        return (($scheme) ? 'http://' : '') . trim(\Yii::$app->request->getServerName(), ',');
    }

    /**
     * @param $url
     * @param bool $scheme
     *
     * @return string
     */
    public static function getDomeinFromUrl($url, $scheme = true)
    {
        $parse = parse_url($url);
        return (($scheme) ? $parse['scheme'] . '://' : '') . $parse['host'];
    }

    /**
     * @param $path
     *
     * @return mixed
     */
    public static function pathToUrl($path, $application = '@frontend')
    {
        return str_replace(\Yii::getAlias($application . '/web/'), '/', $path);
    }

    /** Разбираем hostname
     *
     * @param $url
     *
     * @return
     */
    public static function parseUrl($url)
    {
        //$url = 'www.scanner.sales-for-life.com/index.html';
        $parsedUrl = parse_url($url);
        $hosts     = explode('.', $parsedUrl['path']);

        $hosts = array_reverse($hosts);

        $domains   = ['hosts' => [], 'domain' => '', 'sub' => ''];
        $countHosts = count($hosts);
        if (array_search('www', $hosts)) {
            $countHosts--;
        }
        foreach ($hosts as $index => $host) {
            if ($host !== 'www') {
                $domains['hosts'][] = $host;
                if ($index < $countHosts - 1) {
                    $domains['domain'] = ($domains['domain']) ? $host . '.' . $domains['domain'] : $host;
                } else {
                    $domains['sub'] = $host;
                }
            }
        }

        if ($countHosts === 2) {
            $domains['domain'] = $domains['hosts'][1] . '.' . $domains['hosts'][0];
            $domains['sub'] = '';
        }

        return $domains;
    }
}
