<?php
namespace emilasp\core\helpers;

use ErrorException;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\imagine\Image;

/**
 * Class FileHelper
 * @package emilasp\core\helpers
 */
class FileHelper extends \yii\helpers\FileHelper
{
    /**
     * @param $dir
     * @param bool|false $count
     *
     * @return array
     */
    public static function findFolders($dir, $count = false)
    {
        if (!is_dir($dir)) {
            throw new InvalidParamException('The dir argument must be a directory.');
        }

        $list   = [];
        $handle = opendir($dir);
        if ($handle === false) {
            throw new InvalidParamException('Unable to open directory: ' . $dir);
        }
        while (($file = readdir($handle)) !== false) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            $path = $dir . DIRECTORY_SEPARATOR . $file;
            if (!is_file($path)) {
                if ($count) {
                    $list[$file] = static::findFiles($path);
                } else {
                    $list[] = $file;
                }
            }
        }
        closedir($handle);
        return $list;
    }

    /**
     * @param string $path
     * @param int $mode
     * @param bool|true $recursive
     */
    public static function createDirectory($path, $mode = 0777, $recursive = true)
    {
        $path = \Yii::getAlias($path);
        parent::createDirectory($path, $mode, $recursive);
    }

    /** Получаем отформатиорванный размер файла
     *
     * @param $bytes
     *
     * @return string
     */
    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }

    /**
     * Сохраняем файл из интернета
     *
     * @param string $url
     * @param string|bool $path
     *
     * @return null|string Путь до сохранённого изображения
     */
    public static function saveFileFromInternet($url, $path = false, $pathFile = false)
    {
        if (!$url || (strpos($url, 'p:') === false && strpos($url, 's:') === false)) {
            return null;
        }

        self::createDirectory(\Yii::getAlias($path));

        for ($i = 0; $i < 5; $i++) {
            try {
                if (self::downloadFileFromUrl($url, \Yii::getAlias($pathFile))) {
                    return $pathFile;
                }
            } catch (Exception $e) {
            }
            sleep(1);
        }
        return false;
    }

    /** Загружаем файл по url
     * @param $url
     * @param $savePath
     * @return bool
     */
    public static function downloadFileFromUrl($url, $savePath)
    {
        if (file_exists($savePath)) {
            return $savePath;
        }
        if($image = self::getFileFromUrl($url)) {
            file_put_contents($savePath, $image);
            return true;
        }

        return false;
    }

    /** Получаем файл из интернета
     * @param $url
     * @return mixed
     */
    public static function getFileFromUrl($url)
    {
        $headers[] = 'Accept: image/gif, image/x-bitmap, image/jpeg, image/pjpeg';
        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
        $userAgent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';
        $process   = curl_init($url);
        curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($process, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, 0);
        $return = curl_exec($process);
        //$tt =  curl_error($process);
        curl_close($process);

        return $return;
    }


    /** Получаем ширину и высоту
     * @param $img
     * @param $params
     *
     * @return array
     */
    public static function getSizes($img, $params)
    {
        $size   = $img->getSize();

        if (empty($params['size']) || strpos($params['size'], 'x') === false) {

            $width  = $size->getWidth();
            $height = $size->getHeight();
        } else {
            $sizes  = explode('x', $params['size']);
            $width  = $sizes[0];
            $height = $sizes[1];

            if (!$height && $width) {
                $orgWidth = $size->getWidth();
                $orgHeight = $size->getHeight();

                $percent = $size->getWidth()/$width;
                $height = ceil($size->getHeight()/$percent);
            }

        }
        return ['width' => $width, 'height' => $height];
    }

    /** Метод сохраняет имеющееся изображение в новом формате/размере по указанному пути
     *
     * @param $original
     * @param $cachePathFile
     * @param $params
     * @param $deleteOriginal
     *
     * @return bool
     * @throws ErrorException
     */
    public static function saveImage($original, $cachePathFile, $params, $deleteOriginal = true)
    {
        try {
            $imagine = Image::getImagine();
            $img     = $imagine->open(Yii::getAlias($original));

            $sizes = self::getSizes($img, $params);
            $width = $sizes['width'];
            $height = $sizes['height'];

            if (!empty($params['crop'])) {
                $thumb = self::getImageCrop($img, $width, $height);
            } elseif (!empty($params['aspectRatio'])) {
                $thumb = self::getImageAspectRatio($img, $width, $height);
            } else {
                $width  = empty($width) ? 2000 : $width;
                $height = empty($height) ? 2000 : $height;

                $thumb = self::getImageRatio($img, $width, $height);
            }

            if (!empty($params['watermark']) && $thumb->getSize()->getWidth() > 200) {
                $watermark = Image::getImagine()->open(Yii::getAlias($params['watermarkSrc']));
                $scale     = $params['scaleWatermark'];
                $watermark = self::getImageRatio(
                    $watermark,
                    $thumb->getSize()->getWidth() / $scale,
                    $thumb->getSize()->getHeight() / $scale
                );
                $thumb->paste($watermark, new Point(10, 10));
            }

            $thumb->save($cachePathFile, ['quality' => $params['quality']]);

            if ($deleteOriginal) {
                unlink($original);
            }
        } catch (\Exception $e) {
            //\Yii::getLogger()->log('Error save Image: ' . var_dump($e), Logger::LEVEL_ERROR, 'binary');
            return false;
        }
        return true;
    }

    /**
     * Метод возвращает отресайженное по максимальному ратио и откропленое по размеру изображение
     * в виде объекта ImageInterface
     *
     * @param $img ImageInterface
     * @param $width integer
     * @param $height integer
     *
     * @return ImageInterface
     */
    public static function getImageCrop($img, $width, $height)
    {
        $size          = $img->getSize();
        $ratio         = self::inset(new Box($width, $height), $size->getWidth(), $size->getHeight(), true);
        $thumbnailSize = $size->scale($ratio);

        $img->resize($thumbnailSize);

        $_width  = ($img->getSize()->getWidth() + $width) / 2 - $img->getSize()->getWidth();
        $_height = ($img->getSize()->getHeight() + $height) / 2 - $img->getSize()->getHeight();

        $img->crop(new Point(abs($_width), abs($_height)), new Box($width, $height));
        return $img;
    }


    /**
     * Метод возвращает отресайженое изображение в виде объекта ImageInterface
     *
     * @param $img ImageInterface
     * @param $width integer
     * @param $height integer
     *
     * @return ImageInterface
     */
    public static function getImageAspectRatio($img, $width, $height)
    {
        $imagine   = Image::getImagine();
        $size      = new Box($width, $height);
        $mode      = ImageInterface::THUMBNAIL_INSET;
        $resizeimg = $img->thumbnail($size, $mode);

        $sizeR   = $resizeimg->getSize();
        $widthR  = $sizeR->getWidth();
        $heightR = $sizeR->getHeight();

        $preserve = $imagine->create($size);
        $startX   = $startY = 0;
        if ($widthR < $width) {
            $startX = ($width - $widthR) / 2;
        }
        if ($heightR < $height) {
            $startY = ($height - $heightR) / 2;
        }
        $preserve->paste($resizeimg, new Point($startX, $startY));
        return $preserve;
    }

    /**
     * Метод возвращает отресайженое изображение в виде объекта ImageInterface
     *
     * @param $img ImageInterface
     * @param $width integer
     * @param $height integer
     *
     * @return ImageInterface
     */
    public static function getImageRatio($img, $width, $height)
    {
        $size          = $img->getSize();
        $ratio         = self::inset(new Box($width, $height), $size->getWidth(), $size->getHeight());
        $thumbnailSize = $size->scale($ratio);

        $img->resize($thumbnailSize);

        return $img;
    }

    /**
     * Считаем ратио - ресайзим изображение так что бы оно влазило в прямоугольник, но сохраняло при этом пропорции.
     *
     * @param BoxInterface $size
     * @param integer $_width
     * @param integer $_height
     * @param bool $outset
     *
     * @return mixed
     */
    public static function inset(BoxInterface $size, $_width, $_height, $outset = false)
    {
        $width  = $size->getWidth();
        $height = $size->getHeight();

        if ($outset) {
            $ratio = max([
                $width / $_width,
                $height / $_height,
            ]);
        } else {
            $ratio = min([
                $width / $_width,
                $height / $_height,
            ]);
        }
        return $ratio;
    }
}
