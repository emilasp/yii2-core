<?php
namespace emilasp\core\helpers;

use DateTime;
use DateInterval;
use DatePeriod;

/**
 * Class DateHelper
 * @package emilasp\core\helpers
 */
class DateHelper
{
    public static $weekDaysShortName = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

    /**
     * Получаем массив дат между двумя датами
     *
     * @param string $start
     * @param string $end
     * @param bool   $info
     * @return array
     */
    public static function getDatesByRange(string $start = null, string $end = null, $info = false): array
    {
        $end = date('Y-m-d', strtotime($end . ' +1 day'));

        $days     = [];
        $start    = (new DateTime($start));
        $end      = new DateTime($end);
        $interval = new DateInterval('P1D');
        $period   = new DatePeriod($start, $interval, $end);
        foreach ($period as $dt) {
            if ($info) {
                $days[$dt->format('Y-m-d')] = [
                    'year'    => $dt->format('Y'),
                    'month'   => $dt->format('m'),
                    'day'     => $dt->format('j'),
                    'weekday' => $dt->format('N')
                ];
            } else {
                $days[] = $dt->format('Y-m-d');
            }
        }
        return $days;
    }

}