<?php

namespace emilasp\core\helpers;

use Yii;

/**
 * Class ThemeHelper
 * @package emilasp\core\helpers
 */
class ThemeHelper
{
    /**
     * Устанавливаем дефолтный путь до вьюх
     * Путь в themeMap(config)/namespace
     * themes/themename/widgets/emilasp/user/widgets/authSocial
     *
     * @param        $object
     * @param string $themePath
     * @param string $subFolder
     * @return string
     */
    public static function getAssetPath($object, string $themePath, string $subFolder = 'views'): string
    {
        $class         = new \ReflectionClass($object);
        $pathNamespace = self::getPath($class->getNamespaceName());

        $path = Yii::getAlias($themePath) . DIRECTORY_SEPARATOR . $pathNamespace . DIRECTORY_SEPARATOR . $subFolder;

        if (!$path || !is_dir($path)) {
            $path = '';
        }
        return $path;
    }

    /**
     * Получаем путь из неймспейса
     *
     * @param string $namespace
     *
     * @return string
     */
    private static function getPath(string $namespace): string
    {
        $arrPath    = explode('\\', $namespace);
        $arrPath[0] = 'modules';

        return implode(DIRECTORY_SEPARATOR, $arrPath);
    }
}
