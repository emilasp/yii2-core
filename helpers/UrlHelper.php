<?php
namespace emilasp\core\helpers;

use yii\helpers\Url;

/**
 * Class Url
 * @package emilasp\core\helpers
 * Usage:
 * In index.php
 *
 * Yii::$classMap['yii\helpers\Url'] = Yii::getAlias(__DIR__ .'/../../frontend/modules/users/components/Url.php');
 * $application = new yii\web\Application($config);
 *
 */
class UrlHelper extends Url
{
    /** Переводим строку в валидный вид для url
     *
     * @param $str
     *
     * @return mixed|string
     */
    public static function strTourl($str)
    {
        $str = StringHelper::rus2translit($str);
        $str = strtolower($str);// в нижний регистр
        $str = preg_replace('~[^-a-z0-9_.]+~u', '-', $str);// заменям все ненужное нам на "-"
        $str = preg_replace('/([_-])\\1+/', '$1', $str);
        $str = trim($str, "-");// удаляем начальные и конечные '-'
        return $str;
    }

    /** Проверяем что это url и добавляем ему схему(опционально)
     * @param $url
     * @param string $addScheme
     *
     * @return bool|string
     */
    public static function isUrl($url, $addScheme = 'http://')
    {
        $urlParts = explode('/', $url);
        if (strpos($urlParts[0], '.') === false && !strpos($url, '://')) {
            return false;
        }
        if ($addScheme && !strpos($url, '://') === false) {
            return $addScheme.$url;
        }
        return $url;
    }

    /** Кодируем класс с неймспейсом для отправки ajax
     * @param $class
     *
     * @return mixed
     */
    public static function classEncode($class)
    {
        return str_replace('\\', '-', $class);
    }

    /** Дедируем класс с неймспейсом после ajax
     * @param $class
     *
     * @return mixed
     */
    public static function classDecode($class)
    {
        return str_replace('-', '\\', $class);
    }
}
