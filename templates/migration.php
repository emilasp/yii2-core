<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */

echo "<?php\n";
?>

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
* Class <?= $className ?>
*/
class <?= $className ?> extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
    * UP
    */
    public function up()
    {
        $this->createTable('files_file', [
            'id'         => $this->primaryKey(11),
            'title'      => $this->string(255),
            'name'       => $this->string(255)->notNull(),
            'type'       => $this->string(14),
            'params'     => $this->string(255),
            'object'     => $this->string(128)->notNull(),
            'object_id'  => $this->integer(11)->notNull(),
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_files_file_created_by',
            'files_file',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_files_file_updated_by',
            'files_file',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createIndex('files_model', 'files_file', ['object', 'object_id', 'status']);

        $this->afterMigrate();
    }

    /**
    * DOWN
    */
    public function down()
    {
        $this->dropTable('files_file');

        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
