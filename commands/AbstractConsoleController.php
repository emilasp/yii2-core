<?php

namespace emilasp\core\commands;

use emilasp\core\helpers\FileHelper;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Конттроллер для вывода в консоль начала и конца работы команды
 *
 * Class ConsoleController
 * @package app\commands
 */
abstract class AbstractConsoleController extends Controller
{
    const FONT_COLOR_YELLOW = '%y';
    const FONT_COLOR_GREEN  = '%g';
    const FONT_COLOR_BLUE   = '%b';
    const FONT_COLOR_RED    = '%r';

    public $outBuffer = [];
    public $lockHandle;

    private $startTime;
    private $startMemory;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->on('beforeAction', function ($event) {
            $this->startTime   = microtime(true);
            $this->startMemory = memory_get_usage();

            $this->display("-----SCRIPT START-----", self::FONT_COLOR_YELLOW);
        });
        $this->on('afterAction', function ($event) {
            $time   = round(microtime(true) - $this->startTime, 4);
            $memory = FileHelper::formatSizeUnits(memory_get_usage() - $this->startMemory);

            $this->display("-----SCRIPT FINISHED-----", self::FONT_COLOR_YELLOW);
            $this->display("%BTime  :%B %n%r{$time} sec%r %n");
            $this->display("%BMemory:%B %n%r{$memory}%r %n");
        });
    }

    /**
     * Выводим в консоль
     *
     * @param string $text
     */
    public static function stdoutText(string $text): void
    {
        echo Console::renderColoredString('%G[' . date("H:i:s") . ']%G%B:%B %n' . $text . "\n");
    }

    /**
     * Пишем лог в консоль
     *
     * @param string $text
     * @param string $color
     * @param bool $buffer
     */
    public function display(string $text, $color = '', bool $debug = true): void
    {
        if (!$debug) {
            $this->outBuffer[] = $text;
        } else {
            self::stdoutText($color . $text . $color . '%n');
        }
    }

    /**
     * Блокируем процесс
     *
     * @return bool
     */
    protected function lockProcess(): bool
    {
        $path = Yii::getAlias('@console/runtime/logs/' . $this->id . '-' . ($this->action->id ?? 'default') . '.loc');

        if (!flock($this->lockHandle = fopen($path, 'w'), LOCK_EX | LOCK_NB)) {
            self::display("Already runninng", self::FONT_COLOR_RED);
            return false;
        }
        fwrite($this->lockHandle, 'run');

        self::display("Lock process", self::FONT_COLOR_YELLOW);

        return true;
    }

    /**
     * Разблокируем процесс
     */
    protected function unlockProcess(): void
    {
        flock($this->lockHandle, LOCK_UN);
        fclose($this->lockHandle);

        self::display("Unlock process", self::FONT_COLOR_YELLOW);
    }

    /**
     * After action
     *
     * @param \yii\base\Action $action
     * @param mixed            $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        if ($this->lockHandle) {
            $this->unlockProcess();
        }

        return parent::afterAction($action, $result);
    }
}
