<?php
namespace emilasp\core\commands;

use Yii;
use yii\db\Connection;
use yii\di\Instance;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;

/** Используется для создания и применения миграций из модулей с выбором модуля в консоле.
 *
 * В конфиг консольного приложения добавить следующие настройки:
 *
 * 'controllerMap' => [
 *     'migrate' => [
 *         'class' => 'emilasp\core\commands\MigrateController',
 *         'default' => [
 *             'db'     => 'db',
 *             'module' => 'app',
 *         ],
 *         'modules' => [
 *             'user' => [
 *                 'db'   => 'db',
 *                 'path' => '@vendor/emilasp/user/migrations'
 *             ],
 *             'site'     => ['path' => '@vendor/emilasp/site/migrations'],
 *             'settings' => ['path' => '@vendor/emilasp/settings/migrations'],
 *             'files'    => ['path' => '@vendor/emilasp/files/migrations'],
 *             'im'       => '@vendor/emilasp/im/migrations',
 *             'app'      => '@app/migrations',
 *         ]
 *     ]
 * ],
 *
 * Class MigrateController
 * @package yii\console\controllers
 */
class MigrateController extends \yii\console\controllers\MigrateController
{
    const DEFAULT_DB       = 'db';
    const DEFAULT_MODULE   = 'app';
    const ALL_MODULE_LABEL = 'all';
    const EXIT_CODE_ALL    = 2;

    public $templateFile = '@vendor/emilasp/yii2-core/templates/migration.php';

    public $modules = [];
    public $default = [
        'db'     => self::DEFAULT_DB,
        'module' => self::DEFAULT_MODULE,
    ];
    public $moduleName;
    public $path;

    public $interactive = false;

    private $transaction;

    public function init()
    {
        parent::init();
        $this->configure();
    }

    /**
     * @param int $limit
     * @param string|null $module
     *
     * @return bool
     */
    public function actionUp($limit = 1, $module = null)
    {
        $returnCode = $this->confirmModule(true, $module);
        if ($returnCode === self::EXIT_CODE_NORMAL) {
            parent::actionUp($limit);
        } elseif ($returnCode === self::EXIT_CODE_ALL) {
            foreach ($this->modules as $name => $module) {
                $this->migrationPath = $module['path'];
                $this->db            = $module['db'];

                parent::actionUp($limit);

                $this->stdout($name . "\n", Console::FG_GREEN);
            }
        }
        return $returnCode;
    }

    /**
     * @param int $limit
     * @param string|null $module
     *
     * @return bool
     */
    public function actionDown($limit = 1, $module = null)
    {
        $returnCode = $this->confirmModule(false, $module);

        if ($returnCode === self::EXIT_CODE_NORMAL) {
            parent::actionDown($limit);
        }
        return $returnCode;
    }

    /**
     * @param int $limit
     * @param string|null $module
     *
     * @return bool
     */
    public function actionRedo($limit = 1, $module = null)
    {
        $returnCode = $this->confirmModule(false, $module);
        if ($returnCode === self::EXIT_CODE_NORMAL) {
            parent::actionRedo($limit);
        }
        return $returnCode;
    }

    /**
     * @param string $name
     * @param string|null $module
     *
     * @return bool
     */
    public function actionCreate($name, $module = null)
    {
        $returnCode = $this->confirmModule(false, $module);
        if ($returnCode === self::EXIT_CODE_NORMAL) {
            parent::actionCreate($name);
        }
        return $returnCode;
    }


    /**
     * @param int $limit
     * @param string|null $module
     *
     * @return bool
     */
    public function actionDrop($limit = 100, $module = null)
    {
        /*$returnCode = Console::prompt("Drop ALL?");
        if (in_array(strtolower($returnCode), ['y', 'yes'])) {
            $this->modules = array_reverse($this->modules);
            foreach ($this->modules as $name => $module) {
                $this->migrationPath = $module['path'];
                $this->db            = $module['db'];
                $this->stdout($this->migrationPath . " is DROP\n", Console::FG_GREEN);
                parent::actionDown(10);

                $this->stdout($name . " is DROP\n", Console::FG_GREEN);
            }
            return self::EXIT_CODE_NORMAL;
        }
        return self::EXIT_CODE_ERROR;*/
    }


    /**
     * Приводим к нужному виду конфиг
     */
    private function configure()
    {
        foreach ($this->modules as $name => $module) {
            $this->modules[$name] = [];

            if (is_array($module)) {
                $this->modules[$name]['path'] = $module['path'];
                $this->modules[$name]['db']   = !empty($module['db']) ? $module['db'] : $this->default['db'];
            } else {
                $this->modules[$name]['path'] = $module;
                $this->modules[$name]['db']   = $this->db;
            }

            $this->modules[$name]['db']   = Instance::ensure($this->modules[$name]['db'], Connection::className());
            $this->modules[$name]['path'] = Yii::getAlias($this->modules[$name]['path']);

            if (!is_dir($this->modules[$name]['path'])) {
                FileHelper::createDirectory($this->modules[$name]['path']);
            }
        }
    }

    /** Спрашиваем для какого модуля юзаем миграции
     *
     * @param bool|false $all
     * @param bool|string $module
     *
     * @return bool
     */
    private function confirmModule($all = false, $module = false)
    {
        if (!$module) {
            $message = $this->stdout("Введите наименование модуля:\n", Console::BOLD);

            if ($all) {
                $message .= $this->stdout("- ", Console::FG_CYAN);
                $message .= $this->stdout(self::ALL_MODULE_LABEL . "\n", Console::FG_PURPLE);
            }

            foreach (array_keys($this->modules) as $name) {
                $message .= $this->stdout('- ', Console::FG_CYAN);
                $message .= $this->stdout($name . "\n", Console::FG_BLUE);
            }

            $options          = [
                'default' => $this->default['module'],
            ];
            $this->moduleName = Console::prompt($message, $options);
        } else {
            $this->moduleName = $module;
        }

        if ($this->moduleName === self::ALL_MODULE_LABEL) {
            $this->stdout("\nМиграции будут применены для всех модулей\n", Console::FG_YELLOW);
            return self::EXIT_CODE_ALL;
        } elseif (!isset($this->modules[$this->moduleName])) {
            $this->stdout("\nМодуль не зарегистрирован в конфиге!\n", Console::FG_RED);
            return self::EXIT_CODE_ERROR;
        }

        $this->migrationPath = $this->modules[$this->moduleName]['path'];
        $this->db            = $this->modules[$this->moduleName]['db'];

        return self::EXIT_CODE_NORMAL;
    }

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!$this->module instanceof Connection) {
                $this->db = Instance::ensure($this->db, Connection::className());
            }
            $this->transaction = $this->db->beginTransaction();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param \yii\base\Action $action
     * @param mixed $result
     */
    public function afterAction($action, $result)
    {
        $this->transaction->commit();
        parent::afterAction($action, $result);
    }
}
