<?php
namespace emilasp\core\assets;

use emilasp\core\components\base\AssetBundle;

/**
 * Class GlyphiconAsset
 * @package emilasp\core\assets
 */
class GlyphiconAsset extends AssetBundle
{
    public $sourcePath = '@bower/glyphicons-halflings';

    public $css = [
        'css/glyphicons-halflings',
    ];

    protected $minCssDynamic = false;


    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
