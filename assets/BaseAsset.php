<?php
namespace emilasp\core\assets;

use yii\web\AssetBundle;

/**
 * Class BaseAsset
 * @package emilasp\core\assets
 */
class BaseAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js  = [];
    public $css = ['base.css', 'content-styling.css'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        '\emilasp\core\extensions\jsHelpers\JsHelpersAsset',
        '\emilasp\core\assets\JboxAsset',
        '\emilasp\core\assets\FontAwesomeAsset',
    ];
}
