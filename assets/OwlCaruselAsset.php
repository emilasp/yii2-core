<?php
namespace emilasp\core\assets;

use emilasp\core\components\base\AssetBundle;

/**
 * Class OwlCaruselAsset
 * @package emilasp\core\assets
 */
class OwlCaruselAsset extends AssetBundle
{
    public $sourcePath = '@bower/owl.carousel/dist';
    public $depends    = [];

    public $css = ['assets/owl.carousel', 'assets/owl.theme.green'];
    public $js  = ['owl.carousel'];
    protected $minCssDynamic = false;
}
