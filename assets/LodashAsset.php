<?php

namespace emilasp\core\assets;

use emilasp\core\components\base\AssetBundle;

/** Подключаем JS helper библиотеку LODASH
 * Class LodashAsset
 * @package emilasp\core\assets
 */
class LodashAsset extends AssetBundle
{
    public $sourcePath = '@bower/lodash';

    public $css = [];
    public $js = ['dist/lodash.min'];
}
