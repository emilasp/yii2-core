<?php
namespace emilasp\core\assets;

use emilasp\core\components\base\AssetBundle;

/** Подключаем JS jquery библиотеку AutoCompleteAsset
 * Class AutoCompleteAsset
 * @package emilasp\core\assets
 */
class AutoCompleteAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-auto-complete';

    public $js        = [
        'jquery.auto-complete.min',
    ];
}
