<?php
namespace emilasp\core\assets;

use emilasp\core\components\base\AssetBundle;

/** Подключаем JS jquery библиотеку FancyTree
 * Class FancyTreeAsset
 * @package emilasp\core\assets
 */
class FancyTreeAsset extends AssetBundle
{
    public $sourcePath = '@bower/fancytree';

    public $css         = [
        'dist/skin-bootstrap/ui.fancytree',
        //'dist/skin-lion/ui.fancytree',
    ];
    public $js        = [
        'dist/jquery.fancytree-all',
        //'dist/src/jquery.fancytree.edit.js',
    ];
}
