<?php

namespace emilasp\core\assets;

use emilasp\core\components\base\AssetBundle;

/**
 * Class AdminLteAsset
 * @package emilasp\core\assets
 */
class AdminLteAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $css = ['admin-lte'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}
