<?php
namespace emilasp\core\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package emilasp\core\assets
 */
class ThemeBaseAsset extends AssetBundle
{
    public $depends  = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\bootstrap\BootstrapThemeAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
        '\emilasp\core\extensions\jsHelpers\JsHelpersAsset',
        '\emilasp\core\assets\JboxAsset',
        '\emilasp\core\assets\BaseAsset',
    ];

    public $css = ['site.css'];

    /**
     * INIT
     */
    public function init()
    {
        parent::init();

        $themesPath = Yii::$app->controller->module->themesPath;
        $theme      = Yii::$app->controller->module->theme;

        $this->sourcePath = $themesPath . DIRECTORY_SEPARATOR . $theme . DIRECTORY_SEPARATOR . 'assets';
    }
}
