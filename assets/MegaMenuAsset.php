<?php
namespace emilasp\core\assets;

use emilasp\core\components\base\AssetBundle;

/**
 * Подключаем MegaMenu(Yamm)
 *
 * Class MegaMenuAsset
 * @package emilasp\core\assets
 */
class MegaMenuAsset extends AssetBundle
{
    public $sourcePath = '@bower/yamm3';

    public $css         = [
        'yamm/yamm',
    ];
}
