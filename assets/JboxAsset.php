<?php
namespace emilasp\core\assets;

use emilasp\core\components\base\AssetBundle;

/**
 * Class JboxAsset
 * @package emilasp\core\assets
 */
class JboxAsset extends AssetBundle
{
    public $sourcePath = '@bower/jbox/Source';

    public $css = [
        'jBox',
        'plugins/Image/jBox.Image',
        'plugins/Notice/jBox.Notice',
        'plugins/Confirm/jBox.Confirm'
    ];
    public $js  = [
        'jBox',
        'plugins/Image/jBox.Image',
        'plugins/Notice/jBox.Notice',
        'plugins/Confirm/jBox.Confirm'
    ];
    protected $minCssDynamic = false;


    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
        '\emilasp\core\extensions\jsHelpers\JsHelpersAsset',
    ];
}
