<?php

namespace emilasp\core\assets;

use emilasp\core\components\base\AssetBundle;

/**
 * Class FontAwesomeAsset
 * @package emilasp\core\assets
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';
    public $css        = [
        'fontawesome/web-fonts-with-css/css/fontawesome-all',
    ];
}
