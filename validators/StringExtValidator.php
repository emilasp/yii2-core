<?php

namespace emilasp\core\validators;

use Yii;
use yii\validators\Validator;

/**
 * Валидатор для строк - расширенный
 *
 * Class NumberValidator
 * @package emilasp\core\validators
 */
class StringExtValidator extends Validator
{
    public $isNumber = false;

    public $max = 11;
    public $min = 11;
    public $message = 'Invalid Phone';

    /**
     * @param \yii\base\Model $model
     * @param string          $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if ($value = self::filter($model->{$attribute}, $this->max)) {
            $length = strlen($value);

            if ($value && ($length > $this->max || $length < $this->min)) {
                $model->addError($attribute, $this->message);
            }

            if (!$model->hasErrors($attribute)) {
                $model->{$attribute} = $value;
            }
        }
    }
}
