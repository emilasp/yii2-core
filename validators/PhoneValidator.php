<?php
namespace emilasp\core\validators;

use Yii;
use yii\validators\Validator;

/** Валидатор телефона
 * Class PhoneValidator
 * @package emilasp\core\validators
 */
class PhoneValidator extends Validator
{
    public $max         = 11;
    public $min         = 11;
    public $message     = 'Invalid Phone';

    /**
     * @param \yii\base\Model $model
     * @param string          $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if ($value = self::filter($model->{$attribute}, $this->max)) {
            $length = strlen($value);

            if ($value && ($length > $this->max || $length < $this->min)) {
                $model->addError($attribute, $this->message);
            }

            if (!$model->hasErrors($attribute)) {
                $model->{$attribute} = $value;
            }
        }
    }

    /**
     * Очищаем телефон
     *
     * @param      $value
     * @param null $length
     * @return bool|mixed|string
     */
    public static function filter($value, $length = null)
    {
        $value = preg_replace('/[^0-9]/', '', $value);

        if ($length && $curLength = strlen($value) > $length) {
            $value = substr($value, strlen($value) - $length);
        }

        return $value;
    }
}
