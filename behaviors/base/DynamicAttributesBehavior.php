<?php
namespace emilasp\core\behaviors\base;

use yii;
use yii\base\Behavior;

/** Добавляет динамические атрибуты модели
 *
 * Class DynamicAttributesBehavior
 * @package emilasp\core\behaviors
 */
abstract class DynamicAttributesBehavior extends Behavior
{
    /** @var array наименования атрибутов  */
    public $attributes = [];

    /** Реализация  получения значения атрибута*/
    abstract protected function getAttribute($name);

    /** Реализация установки значения атрибута
     * @param string $value */
    abstract protected function setAttribute($name, $value);

    /** Проверяем на существование свойства
     * @param string $name
     * @param bool|true $checkVars
     *
     * @return bool
     */
    public function canGetProperty($name, $checkVars = true)
    {
        if ($this->checkName($name)) {
            return true;
        }
        return parent::canGetProperty($name, $checkVars);
    }

    /** Проверяем на возможность записи свойства
     * @param string $name
     *
     * @return bool
     */
    public function canSetProperty($name, $checkVars = true)
    {
        if ($this->checkName($name)) {
            return true;
        }
        return parent::canSetProperty($name, $checkVars);
    }

    /** Геттер
     * @param string $name
     *
     * @return array|mixed
     */
    public function __get($name)
    {
        if ($this->checkName($name)) {
            return $this->getAttribute($name);
        }
        return parent::__get($name);
    }

    /** Сеттер
     * @param string $name
     * @param mixed $value
     *
     * @return bool
     */
    public function __set($name, $value)
    {
        if ($this->checkName($name)) {
            $this->setAttribute($name, $value);
            return true;
        }
        parent::__set($name, $value);
    }

    /** Проверяем что это разрешённое свойство
     * @param $name
     *
     * @return bool
     */
    private function checkName($name)
    {
        $allowAttribute = in_array($name, (array)$this->attributes);
        if ($allowAttribute) {
            return true;
        }
        return false;
    }
}
