<?php
namespace emilasp\core\behaviors\base;

use yii;
use yii\base\Behavior;

/** Добавляет динамический атрибут модели
 *
 * Наслудуемся от этого класса:
 *
 *  public function behaviors()
 *  {
 *      return ArrayHelper::merge([
 *          [
 *              'class' => LinkedAttributeBehavior::className(),
 *              'attribute' => 'categoriesId',
 *              'linkedClass' => CategoryLink::className(),
 *              'taxonomyclass' => Category::className(),
 *          ],
 *      ], parent::behaviors());
 *  }
 *
 * Добавляем модели правило валидации:
 * public function rules()
 * {
 *     return [
 *         ['categoriesId', 'safe'],
 *     ];
 * }
 *
 * В форме:
 * $form->field($model, 'categoriesId')->textInput(['id' => 'good-categoriesid'])
 *
 * Class DynamicAttributeBehavior
 * @package emilasp\core\behaviors
 */
abstract class DynamicAttributeBehavior extends DynamicAttributesBehavior
{
    /** @var string наименование атрибута  */
    public $attribute;

    public function init()
    {
        $this->attributes = (array)$this->attribute;
        parent::init();
    }
}
