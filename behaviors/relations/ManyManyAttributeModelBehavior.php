<?php
namespace emilasp\core\behaviors\relations;

use yii;
use yii\base\Behavior;
use yii\helpers\Json;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use emilasp\core\behaviors\base\DynamicAttributeBehavior;

/** Добавляем атрибут с json, где храним id связанных с моделью элементов таксономии
 * Выводим в форме.
 * При сохранении создаём связи.
 *
 *  public function behaviors()
 *  {
 *      return ArrayHelper::merge([
 *          [
 *              'class' => ManyManyAttributeModelBehavior::className(),
 *              'attribute' => 'categoriesId',
 *              'linkedClass' => CategoryLink::className(),
 *              'taxonomyclass' => Category::className(),
 *              'relationName' => 'categories',
 *          ],
 *      ], parent::behaviors());
 *  }
 *
 * Добавляем модели правило валидации:
 * public function rules()
 * {
 *     return [
 *         ['categoriesId', 'safe'],
 *     ];
 * }
 *
 * В форме:
 * $form->field($model, 'categoriesId')->textInput(['id' => 'good-categoriesid'])
 *
 *  В моделе добавляем relation:
 * public function getGoodHasCategory()
 * {
 *     return $this->hasMany(CategoryLink::className(), ['object_id' => 'id'])
 *                 ->where(['object' => self::className()]);
 * }
 * public function getCategories()
 * {
 *     return $this->hasMany(Category::className(), ['id' => 'taxonomy_id'])->via('goodHasCategory');
 * }
 *
 * Class ManyManyAttributeModelBehavior
 * @package emilasp\core\behaviors\relations
 */
class ManyManyAttributeModelBehavior extends DynamicAttributeBehavior
{
    /** @var string имя атрибута */
    public $attribute;
    /** @var string связующий класс */
    public $linkedClass;
    /** @var string класс таксономии */
    public $taxonomyClass;
    /** @var string имя relation в моделе */
    public $relationName;

    private $value;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT  => 'linkedTaxonomy',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'linkedTaxonomy',
        ];
    }

    /** Получаем id'шники связанной таксономии
     * @return string
     */
    protected function getAttribute($name)
    {
        if ($this->value) {
            return $this->value;
        }
        $this->value = Json::encode(array_keys(ArrayHelper::map($this->getTaxonomyLinked(), 'id', 'id')));
        return $this->value;
    }

    /** Устанавливаем значение поля из формы
     *
     * @param string $value
     */
    protected function setAttribute($name, $value)
    {
        $this->value = $value;
    }

    /**
     * Создаём связи
     */
    public function linkedTaxonomy()
    {
        if (!$this->owner->id) {
            return null;
        }
        $modelTaxonomy = $this->taxonomyClass;
        $modelOwner    = $this->owner;
        $ids           = (array) Json::decode($this->value);

        $taxOld    = $this->getTaxonomyLinked();
        $taxOldIds = array_keys(ArrayHelper::map($taxOld, 'id', 'id'));

        /** удаляем лишние связи */
        foreach ($taxOld as $taxItem) {
            if (!in_array($taxItem->id, $ids)) {
                $modelOwner->unlink($this->relationName, $taxItem, true);
            }
        }

        /** добавляем новые */
        foreach ($ids as $id) {
            if (!in_array($id, $taxOldIds)) {
                $modelTax = $modelTaxonomy::findOne($id);
                $modelOwner->link($this->relationName, $modelTax, ['object' => $modelOwner::className()]);
            }
        }
    }

    /** Получаем id связанных таксономий
     * @return string
     */
    private function getTaxonomyLinked()
    {
        $taxonomyItems = $this->owner->{$this->relationName};
        return $taxonomyItems;
    }
}
