<?php
namespace emilasp\core\behaviors\relations;

use yii;
use yii\base\ErrorException;
use emilasp\core\behaviors\base\DynamicAttributeBehavior;

/** Добавляем Relation для модели
 *
 * Class RelationBehavior
 * @package emilasp\core\behaviors\relations
 */
abstract class RelationBehavior extends DynamicAttributeBehavior
{
    const TYPE_HAS_MANY = 'hasMany';
    const TYPE_HAS_ONE  = 'hasOne';

    /** @var string имя класса relation */
    public $relationClass;

    /** @var string имя relation */
    public $relationName;

    /** @var  string имя атрибута модели с которой связываем (object_id) */
    public $linkedAttribute = 'object_id';

    /** @var string Тип связи */
    public $type      = self::TYPE_HAS_MANY;

    /** @var array дополнительные условия для связи */
    public $condition = [];

    public function init()
    {
        $this->checkRequireFields();
        //$this->attribute = $this->relationName;
        parent::init();
    }

    /** Получаем вариации для справочника
     * @return mixed
     */
    protected function getAttribute($name)
    {
        $relationModel = $this->relationClass;
        return $this->owner->{$this->type}(
            $relationModel::className(),
            [
                $this->linkedAttribute => 'id'
            ]
        )->where($this->condition);
    }

    /**
     * @param $name
     * @param string $value
     *
     * @throws ErrorException
     */
    protected function setAttribute($name, $value)
    {
        throw new ErrorException('Read only relation attribute');
    }

    private function checkRequireFields()
    {
        if ($this->relationName === null) {
            throw new ErrorException('Set field relationName is Require.');
        }
        if ($this->relationClass === null) {
            throw new ErrorException('Set field relationClass is Require.');
        }
    }
}
