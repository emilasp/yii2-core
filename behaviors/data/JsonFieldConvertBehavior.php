<?php

namespace emilasp\core\behaviors\data;

use emilasp\core\components\base\ActiveRecord;
use yii;
use yii\base\Behavior;
use yii\db\JsonExpression;

/**
 * Поведение заглушка для работы с JSON полями
 *
 * Class JsonFieldBehavior
 * @package emilasp\core\behaviors
 */
class JsonFieldConvertBehavior extends Behavior
{
    /** Имя поля для сортировки в БД
     * @var string
     */
    public $attributes = [];

    /** @var ActiveRecord */
    public $owner;


    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'convertToObject',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'convertToObject',
            ActiveRecord::EVENT_AFTER_FIND    => 'convertToString',
        ];
    }

    /**
     * Конвертируем в строку
     */
    public function convertToString(): void
    {
        foreach ($this->attributes as $attribute) {
            if (is_array($this->owner->{$attribute}) || !is_object($this->owner->{$attribute})) {
                $this->owner->{$attribute} = json_encode($this->owner->{$attribute});
            }
        }
    }

    /**
     * Конвертируем в строку
     */
    public function convertToObject(): void
    {
        foreach ($this->attributes as $attribute) {
            if (is_string($this->owner->{$attribute})) {
                $this->owner->{$attribute} = new JsonExpression(json_decode($this->owner->{$attribute}, true));
            }
        }
    }

    /**
     * Преобразуем json поля в объект/массив
     */
    public function toJsonField(): void
    {
        foreach ($this->attributes as $attribute) {
            if (is_string($this->owner->{$attribute})) {
                $this->owner->{$attribute} = json_decode($this->owner->{$attribute});
            }
        }
    }
}
