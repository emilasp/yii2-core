<?php

namespace emilasp\core\behaviors\data;

use emilasp\core\components\base\ActiveRecord;
use yii;
use yii\base\Behavior;

/**
 * Поведение позволяет обновлять сортировку для записи внутри таблицы
 *
 * Class OrderBehavior
 * @package emilasp\core\behaviors
 */
class OrderBehavior extends Behavior
{
    /** Имя поля для сортировки в БД
     * @var string
     */
    public $attribute = 'order';

    /** @var ActiveRecord */
    public $owner;


    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'insertModel',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'saveModel',
            //ActiveRecord::EVENT_BEFORE_DELETE => 'deleteModel',
        ];
    }

    /**
     * При создании модели
     */
    public function insertModel()
    {
        /** @var ActiveRecord $model */
        $model     = $this->owner;
        $lastOrder = (int)$model::find()->max('`' . $this->attribute . '`');

        $model->{$this->attribute} = $lastOrder + 1;
    }

    /**
     * При сохранение модели
     */
    public function saveModel()
    {
        if ($this->owner->getDirtyAttributes([$this->attribute])) {
            $oldOrder = $this->owner->getOldAttribute($this->attribute);
            $newOrder = $this->owner->{$this->attribute};

            $table = $this->owner::tableName();

            if ($newOrder > $oldOrder) {
                $sqlUpdate = <<<SQL
                UPDATE {$table} SET `{$this->attribute}`=`{$this->attribute}`-1
                WHERE `{$this->attribute}` > $oldOrder AND `{$this->attribute}` <= $newOrder;
SQL;
            } elseif ($newOrder < $oldOrder) {
                $sqlUpdate = <<<SQL
                UPDATE {$table} SET `{$this->attribute}`= `{$this->attribute}`+1
                WHERE `{$this->attribute}` >= $newOrder AND `{$this->attribute}` < $oldOrder;
SQL;
            }
            $this->owner::getDb()->createCommand($sqlUpdate)->execute();
        }
    }
}
