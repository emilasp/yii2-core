<?php
namespace emilasp\core\behaviors\data;

use emilasp\core\components\base\ActiveRecord;
use yii;
use yii\helpers\Json;
use yii\base\Behavior;

/** @TODO Сохраняем и получаем историю из jsonb поля модели
 * Добавляем повделение после author и date поведений
 *
 * Class HistoryBehavior
 * @package emilasp\core\behaviors
 */
class HistoryBehavior extends Behavior
{
    /** Имя поля в БД
     * @var string
     */
    public $attribute = 'history';

    public $exclude = [];

    public $updatedAtAttribute = 'updated_at';
    public $updatedByAttribute = 'updated_by';

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'createHistory',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'saveHistory',
        ];
    }

    /**
     * Сохраняем новую версию
     */
    public function saveHistory()
    {
        $oldAttributes = $this->owner->oldAttributes;
        $dirty         = [];
        foreach ($this->owner->attributes() as $attribute) {
            if (in_array($attribute, $this->exclude) || $attribute === $this->attribute) {
                continue;
            }
            if ($this->owner->{$attribute} != $oldAttributes[$attribute]) {
                $dirty[$attribute] = $this->owner->{$attribute};
            }
        }

        if (count($dirty)) {
            $dirty[$this->updatedAtAttribute] = date('Y-m-d H:i:s');
            $dirty[$this->updatedByAttribute] = Yii::$app->user->id;

            $rows   = Json::decode($this->owner->{$this->attribute});
            $rows[] = $dirty;

            $this->owner->{$this->attribute} = Json::encode($rows);
        }
    }

    /**
     * Создание первоначальной истории
     */
    public function createHistory()
    {
        if (!$this->owner->{$this->attribute}) {
            $this->owner->{$this->attribute} = '[]';
        }

    }
    /** получаем историю
     *
     * @param null $limit
     */
    public function getHistory($limit = null)
    {

    }

    /** Восстанавливаем историю
     *
     * @param $row
     */
    public function restore($row)
    {

    }
}
