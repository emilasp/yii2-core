<?php
namespace emilasp\core\components\xml;

use Yii;
use yii\base\Component;
use yii\log\Logger;

/**
 * Class XmlParserComponent
 * @package emilasp\core\components
 */
class XmlParserComponent extends Component
{
    public $email = [];

    public $structure = [
        ''
    ];
}
