<?php
namespace emilasp\core\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\log\EmailTarget;

/**
 * Class ErrorEmailTarget
 * @package emilasp\core\components
 */
class ErrorEmailTarget extends EmailTarget
{
    /** @var array исключения по категориям */
    public $exceptCategory = [];

    /** @var bool форматировать сообщения как html */
    public $asHtml = true;

    public $mailComponent = 'mail';

    /**  */
    public function export()
    {
        $this->removeExceptCategory();
        $this->formatMessageHtml();

        $body = implode("<br>", $this->messages);

        $this->sendMail($body);
    }

    /** Отправляем ошибку на почту
     * @param $body
     */
    private function sendMail($body)
    {
        if (strlen($body) > 10) {
            /** @var \yii\swiftmailer\Mailer $mailer */
            $mailer = Yii::$app->{$this->mailComponent};

            $message = $mailer->compose();
            Yii::configure($message, $this->message);
            $message->setHtmlBody($body);
            $message->send();
        }
    }

    /**
     * Удаляем сообщения с категориями из exceptions
     */
    private function removeExceptCategory()
    {
        foreach ($this->messages as $index => $message) {
            foreach ($this->exceptCategory as $except) {
                if (strpos($message[2], $except) !== false) {
                    unset($this->messages[$index]);
                }
            }
        }
    }

    /**
     * Форматируем сообщения в Html
     */
    private function formatMessageHtml()
    {
        if ($this->asHtml) {
            foreach ($this->messages as $key => $message) {
                $message = $this->formatMessage($message);
                $message = explode("\n", $message);

                foreach ($message as $index => $string) {
                    if (!$index) {
                        $message[$index] = Html::beginTag('div', ['style' => 'color:#CC3300']);
                    } else {
                        $message[$index] = Html::beginTag('div', ['style' => 'color:#3366CC']);
                    }
                    $message[$index] .= $string;

                    $message[$index] = str_replace(
                        'exception',
                        '<br><br><span style="font-weight: bold">Exception: </span>',
                        $message[$index]
                    );

                    $message[$index] .= Html::endTag('div');
                }
                $this->messages[$key] = implode('<br>', $message);
            }
        }
    }
}
