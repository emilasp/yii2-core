<?php

namespace emilasp\core\components\gridview;

use kartik\widgets\Select2;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * Class RelationColumn
 * @package emilasp\media\components\gridView
 */
class RelationColumn extends DataColumn
{
    const FILTER_TYPE_SELECT2 = 'SELECT2';

    public $relationName;
    public $route;
    public $name   = 'name';
    public $format = 'raw';
    public $filterConfig;

    /**
     * Поиск для Select2
     *
     * @param null $q
     * @param null $id
     * @return array
     */
    /*
     *
     [
                    'attribute'    => 'counteragent_id',
                    'class'        => RelationColumn::className(),
                    'relationName' => 'counteragent',
                    'name'         => 'name_short',
                    'route'        => '/counteragents/counteragent/view',
                    'filterConfig' => [
                        'type'        => RelationColumn::FILTER_TYPE_SELECT2,
                        'route'       => '/counteragents/counteragent/search',
                    ],
                ],
     *
     *
     * public function actionSearch($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = Counteragent::find();
            $query->select('id, name_short AS text')
                ->where(['ilike', 'name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Counteragent::findOne($id)->name_short];
        }
        return $out;
    }*/

    /**
     * INIT
     */
    public function init()
    {
        parent::init();

        if ($this->filterConfig && $this->filterConfig['type'] === self::FILTER_TYPE_SELECT2) {
            $this->filter = Select2::widget([
                'model'         => $this->grid->filterModel,
                'attribute'     => $this->attribute,
                'pluginOptions' => [
                    'allowClear'         => true,
                    'minimumInputLength' => 3,
                    'language'           => [
                        'errorLoading' => new JsExpression("function () { return 'Ожидайте результатов...'; }"),
                    ],
                    'ajax'               => [
                        'url'      => Url::toRoute([$this->filterConfig['route']]),
                        'dataType' => 'json',
                        'data'     => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                    'templateResult'     => new JsExpression('function(model) { return model.text; }'),
                    'templateSelection'  => new JsExpression('function (model) { return model.text; }'),
                ],
                'options'       => [
                    'placeholder' => 'Начните вводить...'
                ]
            ]);
        }
    }

    /**
     * Формируем вывыод в колонку ссылки
     *
     * @param mixed $model
     * @param mixed $key
     * @param int   $index
     * @return string
     */
    public function renderDataCellContent($model, $key, $index)
    {
        $relation = $this->relationName ? $model->{$this->relationName} : $model;

        if ($relation) {
            $url = Url::toRoute([$this->route, 'id' => $relation->id]);

            return $relation ? Html::a($relation->{$this->name}, $url, ['data-pjax' => 0]) : null;
        }
        return null;
    }
}
