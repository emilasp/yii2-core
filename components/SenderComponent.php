<?php
namespace emilasp\core\components;

use Yii;
use yii\base\Component;
use yii\log\Logger;

/**
 * Class SenderComponent
 * @package emilasp\core\components
 */
class SenderComponent extends Component
{
    public $email = [];

    /**
     * Отправляем почту
     *
     * @param array $adresats ['user@somehost.com', 'John Smith']
     * @param string $subject
     * @param bool|string $view
     * @param bool|array $viewParams
     * @param null|array $attach
     */
    public function sendMail($adresats, $subject, $view, $viewParams = [], $attach = null)
    {
        $mail = Yii::$app->mail->compose(['html' => $view], $viewParams);

        if ($attach) {
            foreach ($attach as $file) {
                $mail->attach($file);
            }
        }

        $send = $mail->setTo($adresats)
                     ->setFrom([$this->email['admin'] => $this->email['adminName']])
                     ->setSubject($subject)
                     ->send();

        if (!$send) {
            Yii::getLogger()->log('Ошибка отправки письма', Logger::LEVEL_ERROR);
        }
    }

    /** Send SMS
     *
     * @param $adresat
     * @param $text
     */
    public function sendSms($adresat, $text)
    {
        $ch = curl_init('http://sms.ru/sms/send');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'api_id' => 'a4842398-b6ac-2604-61ea-ac294e12380b',
            'to'     => '7' . $adresat,
            'text'   => iconv('windows-1251', 'utf-8', $text),
        ]);
        $body = curl_exec($ch);
        curl_close($ch);

        if (!$body) {
            Yii::getLogger()->log('Ошибка отправки sms', Logger::LEVEL_ERROR);
        }
    }

}
