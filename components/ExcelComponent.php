<?php

namespace emilasp\core\components;

use PHPExcel;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Fill;
use PHPExcel_Writer_Abstract;
use PHPExcel_Writer_Excel5;
use yii\base\Component;

/**
 * Class ExcelComponent
 * @package emilasp\core\components
 */
class ExcelComponent extends Component
{
    const FORMAT_XLS  = 'xls';
    const FORMAT_XLSX = 'xlsx';
    const FORMAT_CSV  = 'csv';
    const FORMAT_PDF  = 'pdf';

    private const FORMAT_OPTIONS = [
        self::FORMAT_XLS  => [
            'ext'  => self::FORMAT_XLS,
            'mime' => 'application/vnd.ms-excel'
        ],
        self::FORMAT_XLSX => [
            'ext'  => self::FORMAT_XLSX,
            'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ],
        self::FORMAT_CSV  => [
            'ext'  => self::FORMAT_CSV,
            'mime' => 'text/csv'
        ],
        self::FORMAT_PDF  => [
            'ext'  => self::FORMAT_PDF,
            'mime' => 'application/pdf'
        ],
    ];

    private const TYPE_DOWNLOAD = 1;
    private const TYPE_FILE     = 2;
    private const TYPE_STRING   = 3;


    public $exportType = self::TYPE_FILE;

    public $format = self::FORMAT_XLS;

    public $options = [
        'sheet' => [
            'name' => 'Лист 1'
        ],
        'style' => [
            'header'  => ['width' => 30, 'align' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT],
            'columns' => [
                'A' => ['align' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT],
            ],
        ]
    ];

    private $xls;

    /**
     * Формируем xls двнные
     */
    public function setDataArray(array $models): void
    {
        $this->xls = new PHPExcel();
        $this->xls->setActiveSheetIndex(0);
        $sheet = $this->xls->getActiveSheet();

        $sheet->setTitle($this->options['sheet']['name']);

        foreach ($models as $indexRow => $model) {
            $indexRow++;
            $indexColumn = 'A';
            foreach ($model as $attribute => $value) {
                $sheet->setCellValue("{$indexColumn}{$indexRow}", $value);

                $indexColumn++;


                if ($this->options['style']['header']) {
                    if (!empty($this->options['style']['header']['width'])) {
                        $sheet->getColumnDimension($indexColumn)->setWidth($this->options['style']['header']['width']);
                    }

                    if ($indexRow === 1) {
                        $sheet->getStyle("$indexColumn$indexRow")->getFont()->setBold(true);
                    }
                }

                if (!empty($this->options['style']['columns']['width'])) {
                    $sheet->getColumnDimension($indexColumn)->setWidth($this->options['style']['columns']['width']);
                }

                if (!empty($this->options['style']['columns'][$indexColumn]['align'])) {
                    $sheet->getStyle("$indexColumn$indexRow")->getAlignment()
                        ->setHorizontal($this->options['style']['columns'][$indexColumn]['align']);
                } elseif (!empty($this->options['style']['header']['align'])) {
                    $sheet->getStyle("$indexColumn$indexRow")->getAlignment()
                        ->setHorizontal($this->options['style']['header']['align']);
                }
            }
        }

        // Вставляем текст в ячейку A1
        /* $sheet->setCellValue("A1", 'Таблица умножения');
         $sheet->getStyle('A1')->getFill()->setFillType(
             PHPExcel_Style_Fill::FILL_SOLID);
         $sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('EEEEEE');*/
    }

    /**
     * Скачиваем файл
     */
    public function download(): void
    {
        $mime = self::FORMAT_OPTIONS[$this->format]['mime'];
        $ext  = self::FORMAT_OPTIONS[$this->format]['ext'];

        $fileName = 'export_' . date('Y-m-s_H-s-i') . ".{$ext}";

        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate('D, d M Y H:i:s T'));
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: {$mime}");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Content-Transfer-Encoding: binary");
        header('Pragma: public');

        $objWriter = $this->getWriter();

        $objWriter->save('php://output');

        exit;
    }

    /**
     * Сохраняем в файл
     *
     * @param string $fileName
     * @return string
     */
    public function saveFile(string $fileName): string
    {
        $objWriter = $this->getWriter();
        $objWriter->save($fileName);

        return $fileName;
    }

    /**
     * Получаем writer
     *
     * @return PHPExcel_Writer_Abstract
     */
    private function getWriter(): PHPExcel_Writer_Abstract
    {
        switch ($this->format) {
            case self::FORMAT_XLSX:
                $objWriter = new \PHPExcel_Writer_Excel2007($this->xls);
                break;
            case self::FORMAT_CSV:
                $objWriter = new \PHPExcel_Writer_CSV($this->xls);
                break;
            case self::FORMAT_PDF:
                $objWriter = new \PHPExcel_Writer_PDF($this->xls);
                break;
            default:
                $objWriter = new \PHPExcel_Writer_Excel5($this->xls);
        }
        return $objWriter;
    }
}

