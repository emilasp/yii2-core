<?php
namespace emilasp\core\components\base;

use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/** Базовый класс для JS виджетов
 *
 * Class WidgetJs
 * @package emilasp\core\assets
 */
abstract class WidgetJs extends \emilasp\core\components\base\Widget
{
    public $id;
    /** @var array Настройки js плагина */
    public $pluginOptions = [];

    /** @var array Настройки по умолчанию для JS плагина */
    protected $defaultOptions = [
        'id' => 'tree',
    ];

    /** @var AssetBundle[] Зависимости от ассетов */
    protected $assetsDepend = [];

    public $events = [];

    public function init()
    {
        parent::init();

        if (!empty($this->pluginOptions['id'])) {
            $this->id = $this->pluginOptions['id'];
        }

        $this->registerEvents();

        $this->pluginOptions = ArrayHelper::merge($this->defaultOptions, $this->pluginOptions);
        $this->pluginOptions = Json::encode($this->pluginOptions);

        $this->replaceEvents();

        foreach ($this->assetsDepend as $asset) {
            $asset::register($this->view);
        }

        $this->registerAssets();
    }

    /**
     * Регистрируем события
     */
    private function registerEvents()
    {
        foreach ($this->events as $eventName => $event) {
            $this->pluginOptions[$eventName] = '{{' . $eventName . '}}';
        }
    }

    /**
     * Вставляем обработчики событий
     */
    private function replaceEvents()
    {
        foreach ($this->events as $eventName => $event) {
            $this->pluginOptions = str_replace('"{{' . $eventName . '}}"', new JsExpression($event), $this->pluginOptions);
        }
    }

    /** Регистрируем JS
     * @return mixed
     */
    abstract protected function registerAssets();
}
