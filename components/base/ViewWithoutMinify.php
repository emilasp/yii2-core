<?php
namespace emilasp\core\components\base;

use Yii;
use yii\base\Controller;

/**
 * Class View
 * @package emilasp\core\components
 */
class ViewWithoutMinify extends \yii\web\View
{
    /**
     * @param string $view
     * @param null $context
     *
     * @return string
     */
    protected function findViewFile($view, $context = null)
    {
        $file = parent::findViewFile($view, $context);

        if ($context instanceof Controller) {
            if (!is_file($file)) {
                $file = $context->module->defaultViewPath . DIRECTORY_SEPARATOR . $context->id . DIRECTORY_SEPARATOR . $view . '.php';
            }
        }
        return $file;
    }
}
