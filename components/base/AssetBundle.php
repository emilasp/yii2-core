<?php

namespace emilasp\core\components\base;

use emilasp\core\CoreModule;
use emilasp\core\helpers\ThemeHelper;
use Yii;

/**
 * Class AssetBundle
 * @package emilasp\core\assets
 */
abstract class AssetBundle extends \yii\web\AssetBundle
{
    protected $minJsDynamic  = false;
    protected $minCssDynamic = false;
    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->setupAssets('css', $this->css);
        $this->setupAssets('js', $this->js);
        $this->setThemeSourcePath();
    }

    /**
     * Set up CSS and JS asset arrays based on the base-file names
     *
     * @param string $type whether 'css' or 'js'
     * @param array  $files the list of 'css' or 'js' basefile names
     */
    protected function setupAssets($type, $files = [])
    {
        foreach ($files as $index => $file) {
            if (($this->minCssDynamic && $type === 'css') || ($this->minJsDynamic && $type === 'js')) {
                $this->{$type}[$index] = YII_DEBUG ? "{$file}.{$type}" : "{$file}.min.{$type}";
            } else {
                $this->{$type}[$index] = "{$file}.{$type}";
            }
        }
    }

    /**
     * Устанавливаем путь до темы
     */
    protected function setThemeSourcePath()
    {
        $themePath = CoreModule::THEME_ALIASE . Yii::$app->getModule('core')->theme;

        $themeAssetPath = ThemeHelper::getAssetPath($this, $themePath, 'assets');

        $this->sourcePath = $themeAssetPath ?: $this->sourcePath;
    }
}
