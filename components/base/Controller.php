<?php
namespace emilasp\core\components\base;

use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Base controller
 */
abstract class Controller extends \yii\web\Controller
{
    const AJAX_STATUS_SUCCESS = 1;
    const AJAX_STATUS_ERROR   = 0;

    public $seo;

    public function actions()
    {
        return [
            /*'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],*/
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            return true;
        } else {
            return false;
        }
    }

    /** Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $id
     * @param $modelClass
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id, $modelClass, $seo = false)
    {
        if (($model = $modelClass::findOne($id)) !== null) {
            if ($seo) {
                $url = $model->getUrl();
                if (strpos(Url::current(), $url) === false) {
                    $this->redirect($url);
                }
            }

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Формируем стандартный ответ для ajax запросов
     *
     * @param integer $status
     * @param string  $message
     * @param array   $params
     *
     * @return array
     */
    protected function setAjaxResponse($status, $message = '', $params = [])
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'status'  => $status,
            'message' => $message,
            'data'    => $params
        ];
    }
}
