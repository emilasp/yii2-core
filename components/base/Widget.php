<?php

namespace emilasp\core\components\base;

use emilasp\core\helpers\ThemeHelper;
use Yii;
use emilasp\core\CoreModule;

/**
 * Class Widget
 *
 * Правила для темизирования виджетов модулей:
 *
 * @app/themes/modules/cms/frontend/widgets/CategoryWidget/views
 * Или если такая папка отсутствует, то ищем тему в самом виджете в папке views
 * /widgets/CategoryWidget/views
 *
 * @package emilasp\core\components\base
 */
abstract class Widget extends \yii\base\Widget
{
    /** @var bool включаем режим дебага */
    public $debug = false;

    /**
     * Устанавливаем дефолтный путь до вьюх
     *
     * Путь в themeMap(config)/namespace
     * themes/themename/widgets/emilasp/user/widgets/authSocial
     * @return string
     */
    public function getViewPath()
    {
        $themePath = CoreModule::THEME_ALIASE . Yii::$app->getModule('core')->theme;

        $themeAssetPath = ThemeHelper::getAssetPath($this, $themePath);

        return $themeAssetPath ?: parent::getViewPath();
    }
}
