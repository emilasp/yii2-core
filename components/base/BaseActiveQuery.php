<?php
namespace emilasp\core\components\base;

use Yii;
use yii\base\UnknownPropertyException;
use yii\caching\DbDependency;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class BaseActiveQuery
 * @package emilasp\core\components\base
 */
class BaseActiveQuery extends ActiveQuery
{
    private $cached = [];
    private $map    = [];
    private $drafted;

    /**
     * Скоуп по статусу
     *
     * @param int $status
     * @return $this
     */
    public function byStatus($status = 1)
    {
        return $this->andWhere(['status' => $status]);
    }

    /**
     * Скоуп по автору
     *
     * @param null $id
     * @return $this
     */
    public function byCreatedBy($id = null)
    {
        if (!$id && !Yii::$app->user->isGuest) {
            $id = Yii::$app->user->id;
        }
        $this->andWhere(['created_by' => $id]);
        return $this;
    }

    /**
     * Вернуть результат в виде map массива
     *
     * @param array $map
     *
     * @return $this
     */
    public function map($key = 'id', $value = 'name')
    {
        $this->map = [$key, $value];
        return $this;
    }

    /**
     * Закешировать результат
     *
     * @param null|integer $duration
     * @param string       $attribute
     *
     * @return $this
     */
    public function cache($duration = null, $attribute = 'updated_at')
    {
        $this->cached = [
            'duration'  => $duration ? $duration : null,
            'attribute' => $duration ? null : $attribute,
        ];

        return $this;
    }

    /**
     * Вернуть результат в виде новой сохраненной модели
     *
     * @param array $attributes
     *
     * @return $this
     */
    public function drafted($attributes = [])
    {
        $this->drafted = $attributes;
        return $this;
    }

    /**
     * Включаем режим записи
     *
     * @param null $db
     * @return array|null|\yii\db\ActiveRecord
     */
    public function one($db = null)
    {
        $row = parent::one($db);

        if ($this->drafted !== null && !$row) {
            $row = new $this->modelClass($this->drafted);
            if (isset($row->status)) {
                $row->status = ActiveRecord::STATUS_DRAFTED;
            }
            $row->save(false);
        }
        return $row;
    }

    /**
     * @param null $db
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function all($db = null)
    {
        if ($this->cached) {
            $result = Yii::$app->db->cache(function () use ($db) {
                return parent::all($db);
            }, $this->cached['duration'], $this->getSqlDependency($this->cached['attribute']));
        } else {
            $result = parent::all($db);
        }

        if ($this->map) {
            return ArrayHelper::map($result, $this->map[0], $this->map[1]);
        }
        return $result;;
    }


    /**
     * Формируем SQL зависимость
     *
     * @param $invalidateAttribute
     * @return null|DbDependency
     * @throws \yii\base\Exception
     */
    private function getSqlDependency($invalidateAttribute)
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;
        $table = $model::tableName();

        if (!$model->hasAttribute($invalidateAttribute)) {
            throw new UnknownPropertyException('GetSqlDependency - отсуствует поле в моделе AR');
        }

        return new DbDependency([
            'sql'      => "SELECT MAX({$invalidateAttribute}) FROM {$table};",
            'reusable' => true
        ]);
    }
}
