<?php
namespace emilasp\core\components\base;

use emilasp\core\helpers\FileHelper;
use Yii;
use yii\data\ActiveDataProvider;

//use yii\modelcollection\CollectionBehavior;

/**
 * Class ActiveRecord
 * @package emilasp\core\components\base
 */
abstract class ActiveRecord extends \yii\db\ActiveRecord
{
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;
    const STATUS_DELETED = 2;
    const STATUS_DRAFTED = 3;

    public static $statuses = [
        self::STATUS_ENABLED  => 'enabled',
        self::STATUS_DISABLED => 'disabled',
        self::STATUS_DELETED  => 'deleted',
        self::STATUS_DRAFTED  => 'drafted'
    ];

    /** Задаём тег для кеша
     * @return string
     */
    public function getCacheTag()
    {
        return self::tableName() . '-' . $this->id . ':' . implode('_', func_get_args());
    }

    /**
     * Проверяем имеет ли модель атрибут
     * @param string $attribute
     * @return bool
     */
    public function isAttr($attribute)
    {
        return array_key_exists($attribute, $this->attributes);
    }


    public static function find()
    {
        $query = new BaseActiveQuery(get_called_class());
        //$query->attachBehavior('collection', CollectionBehavior::class);
        return $query;
    }

    /**
     * Эмулируем удаление через статус
     *
     * @param string $attributeDelete
     * @param int    $status
     *
     * @return bool
     */
    public function deleteSafe($attributeDelete = 'status', $status = self::STATUS_DELETED)
    {
        $this->{$attributeDelete} = $status;

        return $this->save();
    }

    /** Получаем путь для модели
     * @param bool|string $basePath
     * @param bool        $create
     *
     * @return string
     */
    public function getUploadPath($basePath = '@common', $create = true)
    {
        $reflect   = new \ReflectionClass($this);
        $modelName = $reflect->getShortName();

        if ($basePath === true) {
            $path = Yii::getAlias('@frontend/web') . DIRECTORY_SEPARATOR;
        } else {
            $path = Yii::getAlias($basePath) . DIRECTORY_SEPARATOR;
        }

        $path .= 'uploads' . DIRECTORY_SEPARATOR . $modelName . DIRECTORY_SEPARATOR . $this->id;

        if (!is_dir($path)) {
            FileHelper::createDirectory($path);
        }
        return $path . DIRECTORY_SEPARATOR;
    }


    /**
     * Поулчаем датапровайдер для поиска по определённым атрибутам
     *
     * @param string $search
     * @param array  $attributes
     * @return ActiveDataProvider
     */
    public function searchByKeyword(string $search, array $attributes = [])
    {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        foreach ($attributes as $attribute) {
            $query->orFilterWhere(['ilike', $attribute, "%{$search}%", false]);
        }

        return $dataProvider;
    }
}
