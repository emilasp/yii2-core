<?php
namespace emilasp\core\components\base;

use Yii;
use yii\base\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class View
 * @package emilasp\core\components
 */
class View extends \rmrevin\yii\minify\View
{
    const PH_BODY_PRE_END  = '<![CDATA[YII-BLOCK-BODY-PRE-END]]>';
    const POS_BODY_PRE_END = 8;

    public $moveHeadToEndBody = false;

    /**
     * @param string $view
     * @param null   $context
     *
     * @return string
     */
    protected function findViewFile($view, $context = null)
    {
        $file = parent::findViewFile($view, $context);

        if ($context instanceof Controller) {
            if (!is_file($file)) {
                $file = $context->module->defaultViewPath . DIRECTORY_SEPARATOR . $context->id . DIRECTORY_SEPARATOR . $view . '.php';
            }
        }
        return $file;
    }

    /**
     * Marks the ending of an HTML page.
     * @param bool $ajaxMode whether the view is rendering in AJAX mode.
     * If true, the JS scripts registered at [[POS_READY]] and [[POS_LOAD]] positions
     * will be rendered at the end of the view like normal scripts.
     */
    public function endPage($ajaxMode = false): void
    {
        $this->trigger(self::EVENT_END_PAGE);

        $this->moveHeadToEndBody();

        $content = ob_get_clean();

        echo strtr($content, [
            self::PH_HEAD         => $this->renderHeadHtml(),
            self::PH_BODY_BEGIN   => $this->renderBodyBeginHtml(),
            self::PH_BODY_PRE_END => $this->renderBodyPreEndHtml(),
            self::PH_BODY_END     => $this->renderBodyEndHtml($ajaxMode),
        ]);

        $this->clear();
    }

    /**
     * @return string
     */
    public function renderBodyPreEndHtml(): string
    {
        $lines = [];
        if (!empty($this->jsFiles[self::POS_BODY_PRE_END])) {
            $lines[] = implode("\n", $this->jsFiles[self::POS_BODY_PRE_END]);
        }
        if (!empty($this->js[self::POS_BODY_PRE_END])) {
            $lines[] = Html::script(implode("\n", $this->js[self::POS_BODY_PRE_END]));
        }

        return empty($lines) ? '' : implode("\n", $lines);
    }

    /**
     * Marks the ending of an HTML body section.
     */
    public function endBodyPreEnd(): void
    {
        echo self::PH_BODY_PRE_END;
    }

    /**
     * Уводим js в низ страницы
     */
    private function moveHeadToEndBody(): void
    {
        if ($this->moveHeadToEndBody) {
            $this->jsFiles[self::POS_BODY_PRE_END] = $this->jsFiles[self::POS_HEAD];

            if (isset($this->jsFiles[self::POS_BEGIN])) {
                $this->jsFiles[self::POS_BODY_PRE_END] = ArrayHelper::merge(
                    $this->jsFiles[self::POS_BODY_PRE_END],
                    $this->jsFiles[self::POS_BEGIN]
                );
            }

            unset($this->jsFiles[self::POS_HEAD]);
        }
    }
}
