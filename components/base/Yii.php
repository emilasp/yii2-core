<?php
use emilasp\core\components\SenderComponent;
use emilasp\users\common\components\UserComponent;
use yii\BaseYii;

/**
 * Yii bootstrap file.
 * Used for enhanced IDE code autocompletion.
 */
class Yii extends BaseYii
{
    /**
     * @var BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}

spl_autoload_register(['Yii', 'autoload'], true, true);
Yii::$classMap = include(__DIR__ . '/../../../../yiisoft/yii2/classes.php');
Yii::$container = new \yii\di\Container();

/**
 * Class BaseApplication
 * Used for properties that are identical for both WebApplication and ConsoleApplication
 *
 * @property yii\sphinx\Connection $sphinx sphinx component
 * @property RbacManager $authManager The auth manager for this application.
 * Null is returned if auth manager is not configured. This property is read-only. Extended component.
 * @property \yii\swiftmailer\Mailer $mailer The mailer component. This property is read-only. Extended component.
 * @property WsConnect $websocket The mailer component. This property is read-only. Extended component.
 * @property SenderComponent $sender The mailer component. This property is read-only. Extended component.
 */
abstract class BaseApplication extends \yii\base\Application
{
}

/**
 * Class WebApplication
 * Include only Web application related components here
 *
 * @property UserComponent $user The user component. This property is read-only. Extended component.
 * @property \yii\web\Response $response The response component. This property is read-only. Extended component.
 * @property \yii\web\ErrorHandler $errorHandler The error handler application component.
 * This property is read-only. Extended component.
 */
class WebApplication extends \yii\web\Application
{
}

/**
 * Class ConsoleApplication
 * Include only Console application related components here
 *
 * @property User $user The user component. This property is read-only. Extended component.
 */
class ConsoleApplication extends \yii\console\Application
{
}
