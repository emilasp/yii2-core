<?php
namespace emilasp\core\debug\panels;

use yii\base\Event;
use yii\base\View;
use yii\base\ViewEvent;
use yii\debug\Panel;

/**
 * Class ViewsPanel
 * @package emilasp\core\debug\panels
 */
class ViewsPanel extends Panel
{
    private $_viewFiles = [];

    public function init()
    {
        parent::init();
        Event::on(View::className(), View::EVENT_BEFORE_RENDER, function (ViewEvent $event) {
            $this->_viewFiles[] = $event->sender->getViewFile();
        });
    }


    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Views';
    }

    /**
     * @inheritdoc
     */
    public function getSummary()
    {
        $url = $this->getUrl();
        $count = count($this->data);
        return "<div class=\"yii-debug-toolbar-block\"><a href=\"$url\">Views <span class=\"label\">$count</span></a></div>";
    }

    /**
     * @inheritdoc
     */
    public function getDetail()
    {
        $rootDir = \Yii::getAlias('@app');
        $rootDir = dirname($rootDir); //remove last dir

        $viewsNormal = [];
        $viewsLayout = [];

        //$this->data = array_unique($this->data); //if delete duplicates
        foreach ($this->data as $view) {
            $viewFile = str_replace($rootDir.DIRECTORY_SEPARATOR, '', $view);
            if (strpos($viewFile, 'layouts') !== false) {
                if (!isset($viewsLayout[$viewFile])) {
                    $viewsLayout[$viewFile] = 0;
                }
                $viewsLayout[$viewFile]++;
            } else {
                if (!isset($viewsNormal[$viewFile])) {
                    $viewsNormal[$viewFile] = 0;
                }
                $viewsNormal[$viewFile]++;
            }
        }

        //Display
        $js = <<<JS
function _openIDE(elem){  
    var xmlhttp = new XMLHttpRequest();     
    xmlhttp.open("GET", "http://localhost:63342/api/file?file=" + elem.innerHTML, true);
    xmlhttp.send(); 
}
JS;
        $content = "<script>$js</script>";

        $content .= 'Views:<ol>';
        foreach ($viewsNormal as $v => $count) {
            $content .= '<li>' . $this->link2IDE($v) . ($count > 1 ? " ($count)" : '') . '</li>';
        }
        $content .= '</ol>';

        $content .= '<hr>on layout:<ol>';
        foreach ($viewsLayout as $v => $count) {
            $content .= '<li>' . $this->link2IDE($v) . ($count > 1 ? " ($count)" : '') . '</li>';
        }
        $content .= '</ol>';

        return $content;
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        return $this->_viewFiles;
    }

    private function link2IDE($linkFile) {
        //Variant 1
        //$port = '63342';
        //return Html::a($linkFile, "http://localhost:$port/api/file?file=$linkFile&line=1", ['target' => '_top']);
        //Variant 2
        return "<a href='#' onclick='_openIDE(this);return false;'>$linkFile</a>";
    }
}
