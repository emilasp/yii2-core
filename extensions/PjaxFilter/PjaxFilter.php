<?php
namespace emilasp\core\extensions\PjaxFilter;

use emilasp\core\components\base\Widget;
use emilasp\goal\common\models\Goal;
use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * Class PjaxFilter
 * @package emilasp\core\extensions\PjaxFilter
 */
class PjaxFilter extends Widget
{
    const TYPE_CHECKBOX_LIST = 'checkboxlist';
    const TYPE_RADIO_LIST    = 'radiolist';
    const TYPE_SELECT        = 'select';

    public $groupId = 'filters';

    public $id;
    public $type      = self::TYPE_CHECKBOX_LIST;

    public $items     = [];
    public $values    = [];
    public $options   = [];

    public $defaultValue;

    public $selectors = [
        'container' => [], // Селекторы блока с фильтрами
        'reload'    => [], // Селекторы блоков для обновления
        'exclude'   => [], // Селекторы фильтров которые нужно исключить из обновления
    ];

    /** @var array выполяем js после применения фильтра */
    public $js = ['after' => ''];

    private $defaultOptions = [
        'class' => 'form-control filter-field',
    ];

    private $isRequest = false;

    public function init()
    {
        $this->registerAssets();

        $this->setDefaultOptions();
        $this->setDataForFilter();
        $this->setFilterValues();
    }

    public function run()
    {
        Pjax::begin(['id' => 'pjax-filter-' . $this->id]);
        echo $this->render($this->type, [
            'id'      => $this->id,
            'name'    => $this->groupId . '[' . $this->id . ']',
            'items'   => $this->items,
            'values'  => $this->values,
            'options' => $this->options,
        ]);

        if ($this->isRequest && $this->js['after']) {
            echo Html::beginTag('script', ['type' => 'text/javascript']) . $this->js['after'] . Html::endTag('script');
        }

        Pjax::end();
    }

    /**
     * Устанавливаем пришежшие значения для фильтра
     */
    public function setFilterValues()
    {
        $groupData    = Yii::$app->request->post($this->groupId);
        $this->values = $groupData[$this->id] ?? [];

        if ($this->values) {
            $this->isRequest = true;
        } elseif ($this->defaultValue) {
            $this->values = [$this->defaultValue];
        }
    }

    /**
     * Получаем пришедшие данные из фильтра
     */
    public static function getFilterData($groupId, $attributes = [])
    {
        $result = [];
        $data = Yii::$app->request->post($groupId);
        foreach ($attributes as $attr => $postAttr) {
            if (isset($data[$attr])) {
                $result[$attr] = $data[$attr];
            }
        }
        return $result;
    }

    /**
     * Устанавливаем атрибуты с селекторами в фильтр
     */
    private function setDataForFilter()
    {
        $data              = [];
        $data['container'] = implode(', ', $this->selectors['container']);
        $data['reload']    = implode(', ', $this->selectors['reload']);
        $data['exclude']   = '#filter-' . implode(', #filter-', $this->selectors['exclude'] ?? []);
        $data['url']       = $this->getUrl();
        $data['field']     = $this->id;

        $this->options['data'] = $data;
    }

    /** Устанавливаем настройки по умолчанию
     */
    private function setDefaultOptions()
    {
        $this->defaultOptions['id'] = 'filter-' . $this->id;
        $this->options              = ArrayHelper::merge($this->defaultOptions, $this->options);
    }

    private function getUrl()
    {
        $parameters = array_merge(
            ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id],
            Yii::$app->request->queryParams,
            ['filter' => $this->id]
        );
        return Url::to($parameters);
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        PjaxFilterAsset::register($view);
    }
}
