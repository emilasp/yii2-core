<?php

namespace emilasp\core\extensions\PjaxFilter;

use emilasp\core\components\base\AssetBundle;

/**
 * Class PjaxFilterAsset
 * @package emilasp\core\extensions\PjaxFilter
 */
class PjaxFilterAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'filter'
    ];
    public $js = [
        'filter'
    ];
}
