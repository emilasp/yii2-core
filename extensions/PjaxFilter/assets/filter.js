$(document).ready(function () {
    $(document).on('change', '.filter-field input, .filter-field select', function () {
        var field = $(this);
        var mainBlock = field.closest('.filter-field');

        var selectorContainer = mainBlock.data('container');
        var selectorReload = mainBlock.data('reload').split(',');
        var selectorExclude = mainBlock.data('exclude');



        var allFilterFields = $(selectorContainer).find('.filter-field');

        var data = {filters:{}};

        allFilterFields.each(function (index) {

            if (!$(this).is(selectorExclude)) {
                var filterInputs = $(this).find('input, select');

                filterInputs.each(function (indexFields) {
                    var inputObj = $(this);
                    var field = inputObj.data('field');
                    var value = inputObj.val();

                    if (typeof data.filters[field] === 'undefined') {
                        data.filters[field] = [];
                    }

                    if (inputObj.is('input[type="checkbox"], input[type="radio"]')) {
                        if (inputObj.is(':checked')) {
                            data.filters[field][data.filters[field].length] = value;
                        }
                    }
                    if (inputObj.is('select')) {
                        if (value) {
                            data.filters[field][data.filters[field].length] = value;
                        }
                    }
                });
            }
        });

        $.each(selectorReload, function( index, value ) {
            $(value).loading(true);
        });
        console.log(data);
        $.ajax({
            type: "POST",
            url: document.location,
            data: $.param(data),
            success: function(msg){
                var document = $(msg);
                $.each(selectorReload, function( index, value ) {
                    var newEl = document.find(value);
                    $(value).replaceWith(newEl);
                });
            }
        });

    })
});