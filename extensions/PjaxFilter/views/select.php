<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$values= $values[0] ?? '';
$items = ArrayHelper::merge(['' => '----'], $items);
?>
<?= Html::beginTag('div', $options) ?>
<?= Html::dropDownList($name, $values, $items, $options) ?>
<?= Html::endTag('div') ?>
