function alert(title, content) {
    var myModal = new jBox('Modal', {
        title:       title,
        content:     content,
        "animation": "flip"
    });
    myModal.open();
}

function notice(content, color) {
    if (typeof content === 'string') {
        content = {message: content};
    }
    for (var message in content) {
        var data = content[message];
        if (typeof data !== 'string') {
            data = data.join(',');
        }
        new jBox('Notice', {
                "title":     "",
                "content":   data,
                "position":  {"x": "right", "y": "top"},
                "color":     color,
                "animation": "tada"
            }
        );
    }
}

$.fn.loading = function(show, position, className){
    position  = typeof position !== 'undefined' ? position : 'prepend';
    className = typeof className !== 'undefined' ? className : 'loading-centered';
    var obj = this;
    if(show){
        $('.loading-centered').remove();
        if (position === 'prepend') {
            obj.prepend('<div class="' + className + '"></div>');
        } else {
            obj.append('<div class="' + className + '"></div>');
        }
        obj.children('.loading-centered').show('fast');
    } else {
        $('.' + className).hide('fast', function () {
            obj.children('.' + className).remove()
        });
    }
};

$(document).on('pjax:start', function (event) {
    var target = $(event.target);
    if (!target.hasClass('no-loading')) {
        target.loading(true);
    }
});

function ajax(link, data, msgTrue, msgFalse, callback) {
    msgTrue  = typeof msgTrue !== 'undefined' ? msgTrue : '';
    msgFalse = typeof msgFalse !== 'undefined' ? msgFalse : '';
    callback = typeof callback !== 'undefined' ? callback : function () {
    };

    var params = {
        type:     'POST',
        url:      link,
        dataType: "json",
        data:     $.param(data),
        success:  function (msg) {

            if (!msgTrue) {
                msgTrue = msg[1];
            }
            if (!msgFalse) {
                msgFalse = msg[1];
            }
            if (msg[0] == '1') {
                notice(msgTrue, 'green');
                callback();
            } else {
                notice(msgFalse, 'red');
            }
        },
        error:    function () {
            notice(msg, 'red');
        }
    };
    if (link) {
        params.url = link;
    }
    $.ajax(params);
}

function updateBlocks(selectors, data, callback) {
    data     = typeof data !== 'undefined' ? data : [];
    callback = typeof callback !== 'undefined' ? callback : function () {
    };
    console.log('update');
    $.ajax({
        type:     'GET',
        //url: link,
        dataType: "html",
        data:     $.param(data),
        success:  function (msg) {
            $.each(selectors, function (index, element) {
                var target  = $(element);
                var newHtml = $(msg).find(element).html();
                target.html(newHtml);
            });
            callback();
        },
        error:    function () {
            notice(msg, 'red');
        }
    });
}


$.fn.uploadPreview = function () {
    var obj = $(this);

    $('<div id="image-holder"> </div>').insertBefore(obj);

    obj.addClass('upload-input-image');

    obj.on('change', function () {
        //Get count of selected files
        var countFiles   = $(this)[0].files.length;
        var imgPath      = $(this)[0].value;
        var extn         = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = $("#image-holder");
        image_holder.empty();

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof (FileReader) != "undefined") {
                //loop for each file selected for uploaded.
                for (var i = 0; i < countFiles; i++) {
                    var reader    = new FileReader();
                    reader.onload = function (e) {
                        $("<img />", {
                            "src":   e.target.result,
                            "class": "upload-thumb-image"
                        }).appendTo(image_holder);
                    }
                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[i]);
                }
            } else {
                alert("This browser does not support FileReader.");
            }
        } else {
            alert("Pls select only images");
        }
    });
};

$(function () {
    new jBox('Image');
});
