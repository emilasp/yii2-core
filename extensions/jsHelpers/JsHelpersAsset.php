<?php
namespace emilasp\core\extensions\jsHelpers;

use emilasp\core\components\base\AssetBundle;
use yii\helpers\Json;
use yii\web\View;


/**
 * Class JsHelpersAsset
 * @package emilasp\core\extensions\jsHelpers
 */
class JsHelpersAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => View::POS_HEAD];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = ['css', 'effects'];
    public $js = ['core', 'effects'];



    /** Ответ при ajax запросе из js хелпера
     * @param $noError
     * @param $messageTrue
     * @param $messageFalse
     *
     * @return string
     */
    public static function response($noError, $messageTrue, $messageFalse)
    {
        if ($noError) {
            $return[] = 1;
            $return[] = $messageTrue;
        } else {
            $return[] = 0;
            $return[] = $messageFalse;
        }
        return Json::encode($return);
    }

}
