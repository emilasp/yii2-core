<?php

namespace emilasp\core\extensions\BModal;

use emilasp\core\components\base\AssetBundle;

/**
 * Class BModalAsset
 * @package emilasp\core\extensions\BModal
 */
class BModalAsset extends AssetBundle
{
    public $sourcePath = '@bower/remodal';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'dist/remodal',
        'dist/remodal-default-theme',
    ];
    public $js = [
        'dist/remodal.min'
    ];
}
