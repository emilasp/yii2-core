<?php
namespace emilasp\core\extensions\BModal;

use emilasp\core\components\base\Widget;
use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class BModal
 * @package emilasp\core\extensions\BModal
 */
class BModal extends Widget
{
    public function init()
    {
        parent::init();
        $this->registerAssets();

        echo Html::beginTag('div', [
            'data-remodal-id' => $this->id,
            'class' => 'remodal',
        ]);

        echo Html::button('×', ['class' => 'remodal-close', 'data-remodal-action' => 'close']);

        ob_start();
    }

    public function run()
    {
        $content = ob_get_clean();

        $content .= Html::endTag('div');

        return $content;
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        BModalAsset::register($view);
    }
}
