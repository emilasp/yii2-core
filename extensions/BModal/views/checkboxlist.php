<?php
use yii\helpers\Html;

if (!isset($options['item'])) {
    $options['item'] = function ($index, $label, $name, $checked, $value) use ($id) {
        return '<div class="checkbox">' . Html::checkbox($name, $checked,
            ['label' => $label, 'value' => $value, 'data-field' => $id]) . '</div>';
    };
}


?>
<?= Html::checkboxList($name, $values, $items, $options) ?>