<?php
namespace emilasp\core\extensions\SideRightMenu;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class MenuAsset
 * @package emilasp\core\extensions\SideRightMenu
 */
class SideRightMenuAsset extends AssetBundle
{
    public $jsOptions = ['position' => View::POS_END];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public $sourcePath = '@vendor/emilasp/yii2-core/extensions/SideRightMenu/assets';

    public $css = [
        'menu.css',
    ];

    public $js = [];
}
