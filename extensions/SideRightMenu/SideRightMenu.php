<?php

namespace emilasp\core\extensions\SideRightMenu;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class TopMenu
 * @package emilasp\core\extensions\SideRightMenu
 */
class SideRightMenu extends Widget
{
    const CACHE_PREFIX = 'user_menu:';

    public $menuPath = '@app/config/menu/';
    public $menuName = 'menu';
    public $menus    = [];

    public $routePrefix;

    private $action;
    private $controller;
    private $module;
    private $userId;

    private $decompositionUrl;

    public function init()
    {
        $this->registerAssets();

        $this->action     = Yii::$app->controller->action->id;
        $this->controller = Yii::$app->controller->id;
        $this->module     = Yii::$app->controller->module->id;
        $this->userId     = (!Yii::$app->user->isGuest) ? Yii::$app->user->id : null;
    }

    public function run()
    {
        $items = include(Yii::getAlias($this->menuPath . $this->menuName . '.php'));
        $items = $this->removeByRight($items);
        $items = $this->setActive($items);

        echo $this->renderMenu($items);
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        SideRightMenuAsset::register($view);
    }


    /** Убираем из списка меню все элементы не проходящие по правам
     *
     * @param $items
     *
     * @return mixed
     */
    private function removeByRight($items)
    {
        foreach ($items as $index => $item) {
            if ($active = $this->isActive($items[$index]['url'])) {
                $items[$index]['active'] = true;
            }

            if (is_array($item)) {
                if (isset($item['items'])) {
                    $items[$index]['items'] = $this->removeByRight($item['items']);

                    if (count($items[$index]['items']) == 0) {
                        unset($items[$index]);
                    }
                } else {
                    if (isset($item['role'])) {
                        $isAllowRole = false;
                        foreach ((array)$item['role'] as $role) {
                            if ($role === '@') {
                                if (!Yii::$app->user->isGuest) {
                                    $isAllowRole = true;
                                }
                            } elseif (Yii::$app->user->can($role)) {
                                $isAllowRole = true;
                            }
                        }
                        if (!$isAllowRole) {
                            unset($items[$index]);
                        }
                    }

                    if (isset($item['rule']) && is_callable($item['rule'])) {
                        $rule = $item['rule'];
                        if ($rule() !== true) {
                            unset($items[$index]);
                        }
                    }
                }
            }
        }
        return $items;
    }


    /** Убираем из списка меню все элементы не проходящие по правам
     *
     * @param $items
     *
     * @return mixed
     */
    private function setActive($items)
    {
        foreach ($items as $index => $item) {
            if ($active = $this->isActive($items[$index]['url'])) {
                $items[$index]['active'] = true;
            }

            if (is_array($item)) {
                if (isset($item['items'])) {
                    $item['items'] = $this->setActive($item['items']);

                    $activeChilds = ArrayHelper::getColumn($item['items'], 'active');
                    foreach ($activeChilds as $activeChild) {
                        if ($activeChild) {
                            $items[$index]['active'] = true;
                        }
                    }
                }
            }
        }
        return $items;
    }


    /**
     * Узнаем активен ли пункт меню
     *
     * @param $route
     * @return bool
     */
    private function isActive($route)
    {
        if ($this->routePrefix) {
            $route = str_replace($this->routePrefix, '', $route);
        }
        $route = $this->decompositionRoute($route);

        if ($this->module === Yii::$app->id) {
            if ($this->controller === $route['module']) {
                if ($this->action === $route['controller'] || !$route['controller']) {
                    return true;
                }
            }
        } elseif ($this->module === $route['module']) {
            if ($this->controller === $route['controller'] || $route['controller'] === null) {
                if ($this->action === $route['action'] || !$route['action'] || $route['action'] === 'index') {
                    return true;
                }
            }
        }

        return false;
    }

    /** Получаем наименования модуля, контроллера и экшена
     *
     * @param $route
     *
     * @return array
     */
    private function decompositionRoute($route)
    {
        //if (!$this->decompositionUrl) {
        $data = [
            'module'     => null,
            'controller' => null,
            'action'     => null,
        ];

        if (strpos($route, '/') !== false) {
            $route = substr($route, 1);
        }
        $dataRoute = explode('/', $route);
        $count     = count($dataRoute);
        for ($i = 0; $i < $count; $i++) {
            switch ($i) {
                case 0:
                    $data['module'] = $dataRoute[$i];
                    break;
                case 1:
                    $data['controller'] = $dataRoute[$i];
                    break;
                case 2:
                    $data['action'] = $dataRoute[$i];
                    break;
            }
        }
        $this->decompositionUrl = $data;
        //}

        return $this->decompositionUrl;
    }

    /**
     * формируем HTML меню
     *
     * @param array $items
     * @return string
     */
    private function renderMenu(array $items): string
    {
        $html = '';

        $html .= Html::beginTag('div', ['class' => 'nav-side-menu']);
        $html .= Html::beginTag('div', ['class' => 'menu-list']);
        $html .= Html::beginTag('ul', ['id' => 'menu-content', 'class' => 'menu-content collapse out']);

        $html .= $this->getHtmlItems($items);

        $html .= Html::endTag('ul');
        $html .= Html::endTag('div');
        $html .= Html::endTag('div');

        return $html;
    }

    /**
     * формируем рекурсивно html айтемов
     *
     * @param array $items
     * @return string
     */
    private function getHtmlItems(array $items): string
    {
        $html = '';
        foreach ($items as $item) {
            if (isset($item['visible']) && !$item['visible']) {
                continue;
            }

            $itemId = $this->getItemId();
            $active = !empty($item['active']) ? ' active' : '';
            $icon   = !empty($item['icon']) ? Html::tag('i', null, ['class' => $item['icon']]) . ' ' : '';


            $linkOptions = $item['linkOptions'] ?? [];

            $item['url'] = $item['url'] !== '#' ? Url::toRoute($item['url']) : $item['url'];

            if (isset($item['items'])) {
                $collapse = ($active || (isset($item['collapse']) && !$item['collapse'])) ? '' : 'collapse';

                $html .= Html::beginTag('li', [
                    'class' => $collapse . 'd' . $active,
                    'data'  => ['toggle' => 'collapse', 'target' => '#' . $itemId]
                ]);

                $html .= Html::a(
                    $icon . $item['label'] . Html::tag('span', '', ['class' => 'arrow']),
                    $item['url'],
                    $linkOptions
                );

                $html .= Html::endTag('li');

                $html .= Html::beginTag('ul', ['id' => $itemId, 'class' => 'sub-menu ' . $collapse]);

                $html .= $this->getHtmlItems($item['items']);

                $html .= Html::endTag('ul');
            } else {
                $html .= Html::beginTag('li', ['class' => $active]);
                $html .= Html::a($icon . $item['label'], $item['url'], $linkOptions);
                $html .= Html::endTag('li');
            }
        }

        return $html;
    }

    /**
     * Формируем item ID
     *
     * @return string
     */
    private function getItemId(): string
    {
        static $id = 0;

        return 'item-' . ++$id;
    }
}
