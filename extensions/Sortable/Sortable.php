<?php
namespace emilasp\core\extensions\Sortable;

use yii\helpers\Html;
use emilasp\core\components\base\WidgetJs;

/**
 * Class Sortable
 * @package emilasp\core\extensions\Sortable
 */
class Sortable extends WidgetJs
{
    public $disableSorted;

    public $pluginOptions = [];

    protected $defaultOptions = [
        'id' => 'tree',
    ];

    protected $assetsDepend = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
    ];

    /**
     * Registers the needed assets
     */
    protected function registerAssets()
    {
        $js = <<<JS
        $('#{$this->id}').sortable({$this->pluginOptions});
JS;

        if ($this->disableSorted) {
            $js .= <<<JS
        $('#{$this->id}').sortable({cancel: "{$this->disableSorted}"});
JS;


        }

        $this->view->registerJs($js);
    }
}
