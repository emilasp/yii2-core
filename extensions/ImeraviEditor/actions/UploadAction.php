<?php
/**
 * This file is part of yii2-imperavi-widget.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://github.com/vova07/yii2-imperavi-widget
 */

namespace emilasp\core\extensions\ImeraviEditor\actions;

use emilasp\core\helpers\StringHelper;
use emilasp\media\models\File;
use vova07\imperavi\actions\UploadFileAction;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\Inflector;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * UploadFileAction for images and files.
 *
 * Usage:
 *
 * ```php
 * public function actions()
 * {
 *     return [
 *         'upload-image' => [
 *             'class' => 'vova07\imperavi\actions\UploadFileAction',
 *             'url' => 'http://my-site.com/statics/',
 *             'path' => '/var/www/my-site.com/web/statics',
 *             'unique' => true,
 *             'validatorOptions' => [
 *                 'maxWidth' => 1000,
 *                 'maxHeight' => 1000
 *             ]
 *         ],
 *         'file-upload' => [
 *             'class' => 'vova07\imperavi\actions\UploadFileAction',
 *             'url' => 'http://my-site.com/statics/',
 *             'path' => '/var/www/my-site.com/web/statics',
 *             'uploadOnlyImage' => false,
 *             'translit' => true,
 *             'validatorOptions' => [
 *                 'maxSize' => 40000
 *             ]
 *         ]
 *     ];
 * }
 * ```
 *
 * @author Vasile Crudu <bazillio07@yandex.ru>
 *
 * @link https://github.com/vova07/yii2-imperavi-widget
 */
class UploadAction extends UploadFileAction
{
    /**
     * @var string Model validator name.
     */
    private $_validator = 'image';

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $file  = UploadedFile::getInstanceByName($this->uploadParam);
            $model = new DynamicModel(['file' => $file]);
            $model->addRule('file', $this->_validator, $this->validatorOptions)->validate();

            if ($model->hasErrors()) {
                $result = [
                    'error' => $model->getFirstError('file'),
                ];
            } else {
                if ($this->unique === true && $model->file->extension) {
                    $model->file->name = uniqid() . '.' . $model->file->extension;
                } elseif ($this->translit === true && $model->file->extension) {
                    $model->file->name = Inflector::slug($model->file->baseName) . '.' . $model->file->extension;
                }

                $object   = Yii::$app->request->post('object');
                $objectId = Yii::$app->request->post('id');
                $modelObj    = $object::findOne($objectId);


                $imageId = $modelObj->addImage(
                    $file,
                    Inflector::slug(
                        $modelObj->seo->keyword . ' '
                        . StringHelper::truncate($model->file->baseName, 20, '')
                    ) . '.' . $model->file->extension
                );

                if ($imageId) {
                    $image = File::findOne($imageId);
                    $url = $image->getUrl(File::SIZE_MID);

                    $result = ['id' => $model->file->name, 'filelink' => $url];

                    if ($this->uploadOnlyImage !== true) {
                        $result['filename'] = $model->file->name;
                    }
                } else {
                    $result = [
                        'error' => Yii::t('vova07/imperavi', 'ERROR_CAN_NOT_UPLOAD_FILE'),
                    ];
                }
            }

            return $result;
        } else {
            throw new BadRequestHttpException('Only POST is allowed');
        }
    }
}
