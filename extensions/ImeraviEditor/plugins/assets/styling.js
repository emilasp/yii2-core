(function ($) {
    $.Redactor.prototype.styling = function () {
        return {
            init: function () {
                var fonts = [
                    {name: 'Info', class: 'content-info'},
                    {name: 'Success', class: 'content-success'},
                    {name: 'Warning', class: 'content-warning'},
                    {name: 'Question', class: 'content-question'},
                    {name: 'Error', class: 'content-error'},
                ];
                var that = this;
                var dropdown = {};

                $.each(fonts, function (i, item) {
                    dropdown['s' + i] = {
                        title: item.name, func: function () {
                            that.styling.set(item);
                        }
                    };
                });

                var button = this.button.add('styling', 'Add style');
                this.button.addDropdown(button, dropdown);
                this.button.setAwesome('styling', 'fa-bookmark');

            },
            set:  function (item) {
                this.buffer.set();
                this.block.setAttr('class', item.class);
            }
        };
    };
})(jQuery);