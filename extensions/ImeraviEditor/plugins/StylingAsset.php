<?php

namespace emilasp\core\extensions\ImeraviEditor\plugins;

use yii\web\AssetBundle;
use yii\web\View;


/**
 * Class StylingAsset
 * @package emilasp\core\extensions\ImeraviEditor\plugins
 */
class StylingAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => View::POS_END];

    public $js = [
        'styling.js'
    ];
}
