<?php

namespace emilasp\core\extensions\ImeraviEditor;

use conquer\codemirror\CodemirrorAsset;
use dosamigos\ckeditor\CKEditor;
use emilasp\core\helpers\FileHelper;
use function update_blog_public;
use vova07\imperavi\Widget;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class ImeraviEditor
 * @package emilasp\core\extensions\ImeraviEditor
 */
class ImperaviEditor extends Widget
{
    public $defaultSettings = [
        'lang'      => 'ru',
        'minHeight' => 200,
        'plugins'   => [
            'clips',
            'counter',
            'definedlinks',
            'limiter',
            'table',
            'video',
            'filemanager',
            'imagemanager',
            'fullscreen',
        ],

        'formattingAdd' => [
            'red-p-add' => [
                'title' => 'Red Paragraph',
                'api' => 'module.block.format',
                'args' => [
                    'tag' => 'p',
                    'class' => 'red-styled',
                ],
            ]
        ],
        'replaceDivs' => false,
    ];

    public $plugins = [
        'styling' => 'emilasp\core\extensions\ImeraviEditor\plugins\StylingAsset',
    ];
}
