<?php

namespace emilasp\core\extensions\CkeEditor\actions;

use emilasp\core\extensions\CkeEditor\CkeEditorWidget;
use Yii;
use yii\base;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * Экшен для загрузки изображений во временную папку
 *
 * Class UploadAction
 * @package app\modules\shop\actions
 */
class UploadAction extends base\Action
{
    /**
     * RUN
     *
     * @return string
     */
    public function run()
    {
        $pathToSave = CkeEditorWidget::getTmpPath(true);

        FileHelper::createDirectory($pathToSave);

        $file     = UploadedFile::getInstanceByName('upload');
        $filename = $file->baseName . '-' . time() . '.' . strtolower($file->extension);

        $file->saveAs($pathToSave . $filename);

        $url     =  CkeEditorWidget::getTmpPath() . $filename;
        $funcNum = Yii::$app->request->get('CKEditorFuncNum');
        $message = 'file uploaded';

        return "<script type='text/javascript'>
                    window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message')
                </script>";
    }
}
