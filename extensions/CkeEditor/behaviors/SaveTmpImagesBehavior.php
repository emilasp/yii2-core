<?php

namespace emilasp\core\extensions\CkeEditor\behaviors;

use emilasp\core\components\base\ActiveRecord;
use emilasp\core\extensions\CkeEditor\CkeEditorWidget;
use emilasp\core\helpers\FileHelper;
use emilasp\core\helpers\StringHelper;
use ReflectionClass;
use yii;
use yii\base\Behavior;

/**
 * Сохраняем временные файлы по новым путям
 *
 * Class SaveTmpImagesBehavior
 * @package emilasp\core\extensions\CkeEditor\behaviors
 */
class SaveTmpImagesBehavior extends Behavior
{
    private const REGULAR_EXPRESSION_SRC = '/alt\s*=\s*"(.*?)".*?src\s*=\s*"(.+?)"/';

    /**  @var array Имя полей в БД */
    public $attributes = ['text'];

    /** @var string Атрибут с наименованием модели */
    public $titleAttribute = 'name';

    /** @var  string текущий атрибут */
    private $attribute;

    /**
     * INIT
     *
     * @return array
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'replaceTmpImages',
            ActiveRecord::EVENT_AFTER_UPDATE => 'replaceTmpImages',
        ];
    }

    /**
     * Сохраняем временные url в новые папки
     */
    public function replaceTmpImages(): void
    {
        foreach ($this->attributes as $attribute) {
            $this->attribute = $attribute;

            $this->processLinks();
        }
    }

    /**
     * Парсим временные ссылки
     */
    private function processLinks(): void
    {
        $content  = $this->owner->{$this->attribute};
        $basePath = CkeEditorWidget::getTmpPath();

        $files = [];
        if (strpos($content, $basePath) !== false) {
            preg_match_all(self::REGULAR_EXPRESSION_SRC, $content, $files);

            $this->moveImages($files[2] ?? [], $files[1] ?? []);

            $this->owner->save();
        }
    }

    /**
     * Перемещаем изображения и чистим временную папку
     *
     * @param array $filenames
     * @param array $alts
     */
    private function moveImages(array $filenames, array $alts): void
    {
        $basePath    = CkeEditorWidget::getTmpPath();
        $basePathAbs = CkeEditorWidget::getTmpPath(true);

        foreach ($filenames as $index => $filename) {
            if (strpos($filename, CkeEditorWidget::TMP_ALIAS_PATH) === false) {
                continue;
            }

            $filename = basename($filename);

            $oldPath    = $basePath . $filename;
            $absOldPath = $basePathAbs . $filename;

            if (is_file($absOldPath)) {
                $newPath    = $this->getNewPath($filename, $alts[$index]);
                $absNewPath = Yii::getAlias('@backend/web' . $newPath);

                if (copy($absOldPath, $absNewPath)) {
                    $content                         = $this->owner->{$this->attribute};
                    $this->owner->{$this->attribute} = str_replace($oldPath, $newPath, $content);
                }
            }
        }

        FileHelper::removeDirectory($basePathAbs);
    }

    /**
     * Формируем новый путь
     *
     * @param string $filename
     * @param string $alt
     * @return string
     */
    private function getNewPath(string $filename, string $alt): string
    {
        $path    = CkeEditorWidget::ALIAS_PATH . DIRECTORY_SEPARATOR;
        $path    .= (new ReflectionClass($this->owner))->getShortName() . DIRECTORY_SEPARATOR . $this->owner->id;
        $absPath = Yii::getAlias('@backend/web' . $path) . DIRECTORY_SEPARATOR;

        FileHelper::createDirectory($absPath);

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $index = 0;
        do {
            if ($alt) {
                $filename = StringHelper::generateName($alt);
            } else {
                $filename = StringHelper::generateName($this->owner->{$this->titleAttribute});
            }

            if ($index) {
                $filename .= '_' . $index;
            }
            $index++;
        } while (is_file($absPath . $filename . '.' . $extension));

        return $path . DIRECTORY_SEPARATOR . $filename . '.' . $extension;
    }
}
