<?php

namespace emilasp\core\extensions\CkeEditor;

use conquer\codemirror\CodemirrorAsset;
use dosamigos\ckeditor\CKEditor;
use emilasp\core\helpers\FileHelper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class CkeEditorWidget
 * @package emilasp\core\extensions\CkeEditor
 */
class CkeEditorWidget extends CKEditor
{
    public const TMP_ALIAS_PATH = '/media/tmp';
    public const ALIAS_PATH     = '/media/uploads';
    public const TMP_URL_PATH   = '/media/tmp';
    public const WEB_ROOT   = '@backend/web';

    /**
     * Формируем пути до временной папки
     *
     * @param bool $absolute
     * @return string
     */
    public static function getTmpPath(bool $absolute = false): string
    {
        $path    = CkeEditorWidget::TMP_ALIAS_PATH . DIRECTORY_SEPARATOR;
        $pathAbs = Yii::getAlias(self::WEB_ROOT) . CkeEditorWidget::TMP_ALIAS_PATH . DIRECTORY_SEPARATOR;

        $userId  = Yii::$app->user->id ?? 'guest';
        $path    .= $userId . DIRECTORY_SEPARATOR;
        $pathAbs .= $userId . DIRECTORY_SEPARATOR;

        if (!is_dir($pathAbs)) {
            FileHelper::createDirectory($pathAbs);
        }

        if ($absolute) {
            return $pathAbs;
        }

        return $path;
    }
}
