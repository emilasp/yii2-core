$( document ).ajaxComplete(function() {
    var progressBarContainer = $('.progress-container');
    var progressBar = progressBarContainer.find('.progress-bar-line');
    progressBar.stop().css('width', '0%');
});

$( document ).ajaxStop(function() {
    var progressBarContainer = $('.progress-container');
    var progressBar = progressBarContainer.find('.progress-bar-line');
    progressBar.stop().css('width', '0%');
});

$( document ).ajaxStart(function() {
    var progressBarContainer = $('.progress-container');
    var progressBar = progressBarContainer.find('.progress-bar-line');
    progressBar.stop().delay(10).queue(function(next){
        $(this).css('width', '50%');
        next();
    }).delay(20).queue(function(next){
        $(this).css('width', '70%');
        next();
    }).delay(30).queue(function(next){
        $(this).css('width', '80%');
        next();
    }).delay(40).queue(function(next){
        $(this).css('width', '85%');
        next();
    }).delay(50).queue(function(next){
        $(this).css('width', '90%');
        next();
    }).delay(100).queue(function(next){
        $(this).css('width', '95%');
        next();
    });
});

