<?php

namespace emilasp\core\extensions\AjaxProgressLine;

use emilasp\core\components\base\AssetBundle;
use yii\web\View;

/**
 * Class AjaxProgressLineAsset
 * @package emilasp\core\extensions\AjaxProgressLine
 */
class AjaxProgressLineAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => View::POS_READY];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'progress-line'
    ];
    public $js = [
        'progress-line'
    ];
}
