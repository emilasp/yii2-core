<?php
namespace emilasp\core\extensions\AjaxProgressLine;

use emilasp\core\components\base\Widget;
use emilasp\goal\common\models\Goal;
use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * Class AjaxProgressLine
 * @package emilasp\core\extensions\AjaxProgressLine
 */
class AjaxProgressLine extends Widget
{
    public function init()
    {
        $this->registerAssets();
        parent::init();
    }

    public function run()
    {
        echo Html::beginTag('div', ['class' => 'progress-container']);
        echo Html::tag('div', '', ['class' => 'progress-bar-line',  'style' => 'width: 0%;']);
        echo Html::endTag('div');
    }
    /**
     * Register client assets
     */
    private function registerAssets()
    {
        AjaxProgressLineAsset::register($this->getView());
    }
}
