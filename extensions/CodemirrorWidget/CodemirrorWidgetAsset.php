<?php
namespace emilasp\core\extensions\CodemirrorWidget;

use emilasp\core\components\base\AssetBundle;

/**
 * Class CodemirrorWidgetAsset
 * @package emilasp\core\extensions\CodemirrorWidget
 */
class CodemirrorWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $css = [
        'codemirror'
    ];
}
