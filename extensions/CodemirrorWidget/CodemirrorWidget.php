<?php
namespace emilasp\core\extensions\CodemirrorWidget;

use conquer\codemirror\CodemirrorAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class CodemirrorWidget
 * @package emilasp\core\extensions\CodemirrorWidget
 */
class CodemirrorWidget extends \conquer\codemirror\CodemirrorWidget
{
    const TYPE_CODE_MARKDOWN = 9;
    const TYPE_CODE_PHP      = 10;
    const TYPE_CODE_JS       = 11;
    const TYPE_CODE_CSS      = 12;
    const TYPE_CODE_SQL      = 13;
    const TYPE_CODE_XML      = 14;
    const TYPE_CODE_HTML     = 15;
    const TYPE_CODE_BASH     = 16;
    const TYPE_TEXT          = 1;

    public static $types = [
        self::TYPE_CODE_MARKDOWN => 'Markdown',
        self::TYPE_CODE_PHP      => 'Php',
        self::TYPE_CODE_HTML     => 'Html',
        self::TYPE_CODE_JS       => 'Js',
        self::TYPE_CODE_CSS      => 'Css',
        self::TYPE_CODE_SQL      => 'Sql',
        self::TYPE_CODE_XML      => 'Xml',
        self::TYPE_CODE_BASH     => 'Bash',
        self::TYPE_TEXT          => 'Text',
    ];

    public $type = self::TYPE_CODE_PHP;

    public $template = '{beginLabel}{labelTitle}
<a role="button" data-toggle="collapse" href="#markHelp" aria-expanded="false" aria-controls="collapseExample"> ?</a>
 {endLabel}{beginWrapper}{input}{hint}{error}{endWrapper}';

    /**
     * Init
     */
    public function init()
    {
        parent::init();

        $this->presetsDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'presets';
        $this->setPresetByType();

        $this->registerAsset();
    }

    /**
     * RUN
     */
    public function run()
    {
        if ($this->type === self::TYPE_CODE_MARKDOWN) {
            $this->field->template = $this->template;
            echo $this->render('markdown-helper');
        }
        parent::run();
    }

    /**
     * Registers Assets
     */
    public function registerAssets()
    {
        $view     = $this->getView();
        $id       = $this->options['id'];
        $settings = $this->settings;
        $assets   = $this->assets;
        if ($this->preset) {
            $preset = $this->getPreset($this->preset);
            if (isset($preset['settings'])) {
                $settings = ArrayHelper::merge($preset['settings'], $settings);
            }
            if (isset($preset['assets'])) {
                $assets = ArrayHelper::merge($preset['assets'], $assets);
            }
        }
        $settings = Json::encode($settings);
        $js       = "
        codemirror = CodeMirror.fromTextArea(document.getElementById('$id'), $settings);
        codemirror.on('change', function() {
           $('#{$id}').val(codemirror.getValue());
           console.log($('#{$id}').val());
        });
        ";
        $view->registerJs($js);
        CodemirrorAsset::register($this->view, $assets);
    }


    /**
     * Устанавливаем пресет по типу
     */
    private function setPresetByType()
    {
        if (!isset(self::$types[$this->type])) {
            $this->type = self::TYPE_TEXT;
        }
        $this->preset = self::$types[$this->type];
    }

    /**
     * Register client assets
     */
    private function registerAsset()
    {
        CodemirrorWidgetAsset::register($this->getView());
    }
}
