<?php
use conquer\codemirror\CodemirrorAsset;

return [
    'assets'   => [
        CodemirrorAsset::MODE_SQL,
        CodemirrorAsset::ADDON_EDIT_MATCHBRACKETS,
        CodemirrorAsset::ADDON_CONTINUECOMMENT,
        CodemirrorAsset::ADDON_COMMENT,
    ],
    'settings' => [
        'lineNumbers'      => true,
        'matchBrackets'    => true,
        'mode'           => "text/x-sql",
        'continueComments' => "Enter",
        'extraKeys'        => ["Ctrl-/" => "toggleComment"],
    ],
];