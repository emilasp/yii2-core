<?php
use conquer\codemirror\CodemirrorAsset;

return [
    'assets'   => [
        CodemirrorAsset::MODE_JAVASCRIPT,
        CodemirrorAsset::ADDON_EDIT_MATCHBRACKETS,
        CodemirrorAsset::ADDON_CONTINUECOMMENT,
        CodemirrorAsset::ADDON_COMMENT,
    ],
    'settings' => [
        'lineNumbers'      => true,
        'matchBrackets'    => true,
        'mode'           => "text/javascript",
        'continueComments' => "Enter",
        'extraKeys'        => ["Ctrl-/" => "toggleComment"],
    ],
];