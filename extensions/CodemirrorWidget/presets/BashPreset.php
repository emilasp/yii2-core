<?php
use conquer\codemirror\CodemirrorAsset;
use yii\web\JsExpression;

return [
    'assets'   => [
        CodemirrorAsset::MODE_SHELL,
        CodemirrorAsset::ADDON_COMMENT,
        CodemirrorAsset::ADDON_DISPLAY_FULLSCREEN,
        CodemirrorAsset::THEME_ECLIPSE,
    ],
    'settings' => [
        'lineNumbers'    => true,
        'matchBrackets'  => true,
        'indentUnit'     => 4,
        'indentWithTabs' => true,
        'mode'           => "text/x-sh",
        'extraKeys'      => [
            "F11" => new JsExpression("function(cm){cm.setOption('fullScreen', !cm.getOption('fullScreen'));}"),
            "Esc" => new JsExpression("function(cm){if(cm.getOption('fullScreen')) cm.setOption('fullScreen', false);}"),
        ],
    ],
];